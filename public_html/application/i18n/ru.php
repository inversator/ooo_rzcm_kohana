<?php defined('SYSPATH') or die('No direct script access.');

return array(
    ':field must contain only letters' => 'Поле ":field" должно содержать только буквы',
    ':field must contain only numbers, letters and dashes' => 'Поле ":field" должно содержать только буквы, цифры и подчеркивания',
    ':field must contain only letters and numbers' => 'Поле ":field" должно содержать только буквы и цифры',
    ':field must be a color' => 'Поле ":field" должно быть кодом цвета',
    ':field must be a credit card number' => 'Поле ":field" должно быть номером кредитной карты',
    ':field must be a date' => 'Поле ":field" должно быть датой',
    ':field must be a decimal with :param2 places' => 'Поле ":field" должно быть десятичным числом с :param2 количеством цифр',
    ':field must be a digit' => 'Поле ":field" должно быть целым числом',
    ':field must be a email address' => 'Поле ":field" должно быть адресом электронной почты',
    ':field must contain a valid email domain' => 'Поле ":field" должно быть существующим адресом электронной почты',
    ':field must equal :param2' => 'Поле ":field" должно быть идентичным :param2',
    ':field must be exactly :param2 characters long' => 'Поле ":field" должно быть длиной :param2 символов',
    ':field must be one of the available options' => 'Поле ":field" должно быть одним из параметров',
    ':field must be an ip address' => 'Поле ":field" должно быть ip-адресом',
    ':field must be the same as :param2' => 'Поле ":field" должно равняться :param2',
    ':field must be at least :param2 characters long' => 'Поле ":field" не должно быть короче :param2 символов',
    ':field must not exceed :param2 characters long' => 'Поле ":field" не должно быть длиннее :param2 символов',
    ':field must not be empty' => 'Поле ":field" не должно быть пустым',
    ':field must be numeric' => 'Поле ":field" должно быть числом',
    ':field must be a phone number' => 'Поле ":field" должно быть номером телефона',
    ':field must be within the range of :param2 to :param3' => 'Поле ":field" должно находиться между :param2 и :param3',
    ':field does not match the required format' => 'Поле ":field" не соответствует требуемому формату',
    ':field must be a url' => 'Поле ":field" должно быть URL-адресом',
    ':field must be an email address' => 'Поле ":field" должно быть email адресом',
    ':field must be the same as :param3' => 'Поле ":field" должно совпадать с полем ":param3"',
    ':field must consist of latin characters and digits' => 'Поле ":field" должно состоять из латиницы и цифр',
    ':field :value busy' => ':field ":value" уже есть',


    'user' => 'Юзер',
    'name' => 'Имя',
    'phone' => 'Телефон',
    'alias' => 'Название',
    'username' => 'Логин',
    'email' => 'Email',
    'message' => 'Текст сообщения',
    'pTitle' => 'Название страницы',
    'bTitle' => 'Заголовок в браузере',
    'password_confirm' => 'Подтверждение пароля',
    'password' => 'Пароль',
    'Previous' => 'Предыдущая страница',
    'First' => 'В начало',
    'Next' => 'Следующая страница',
    'Last' => 'К последним',

    'company' => 'компания',
    'catalog' => 'каталог',
    'products' => 'каталог',

    'about company' => 'о компании',
    'history' => 'история',
    'The plant' => 'завод',
    'The staff' => 'сотрудники',
    'certificates' => 'лицензии',
    'industrial safety' => 'промбезопасность',
    'vacancy' => 'вакансии',
    'requisites' => 'реквизиты',

    'lead' => 'свинец',
    'polypropylene' => 'полипропилен',
    'cooperation' => 'сотрудничество',
    'for сustomers' => 'покупателям',
    'for suppliers' => 'поставщикам',
    'ecology' => 'экология',
    'news' => 'новости',
    'contact us' => 'контакты',

    'Navigation' => 'Навигация',
    'Contact Information' => 'Контактная информация',
    'Russian supplier of lead and lead alloys' => 'Российский поставщик свинца и свинцовых сплавов',
    'Policy regarding the processing of personal data' => 'Политика в отношении обработки персональных данных',

    'Callback' => 'Перезвоните мне',

    '305025, Kursk, Directions Magistralny, d. 18T, office 5' => '305025, г. Курск, Проезд Магистральный, д. 18Т, офис 5',
    'Office hours :  9:00 - 18:00' => 'Режим работы: c 9 до 18',

    'We will be glad to start cooperation with you!' => 'Мы будем рады начать сотрудничество с Вами!',

    'We will call you back!' => 'Мы перезвоним Вам!',
    'Leave your phone number and we will get back to you as soon as possible!' => 'Оставьте свой номер телефона и мы свяжемся с Вами в ближайшее время!',
    'Send' => 'Отправить',
    'Close' => 'Закрыть',

    'Ask' => 'Спросить',
    'Your question' => 'Ваш вопрос',
    'Your name' => 'Ваше имя',
    'Your phone' => 'Ваш телефон',
    'Your email' => 'Ваш email',
    'Ask a Question' => 'Задать вопрос',
    'Here you can ask a question of interest. Our managers will contact you as soon as possible.' => 'Здесь Вы можете задать интересующий вопрос. Наши менеджеры свяжутся с Вами ближайшее время.',

    'High quality refined lead, lead alloys, calcium alloys.' => 'Cоответствует мировым стандартам',
    'All goods comply with ISO standards.' => 'Высококачественный свинец марки С2С, свинцовые сплавы УС2, ССуА, ССуС, кальциевые сплавы',

    'High quality products' => 'Высокое качество продукции',
    'All products meet the world and Russian quality standards' => 'Вся продукция соответствует мировым и российским стандартам качества',

    '<p>
	 Ltd «Ryazcvetmet» is one of the major Russian lead producers. For its more than 55-year history the plant «Ryazcvetmet» has become the leader оf Russian metallurgy.
</p>
<p>
	 We have regular production of refined lead. We also produce the traditional lead alloys.
</p>
<p>
	 There is a modernly equipped laboratory in the factory, that allows us to distribute our products in compliance with both, the global market requirements and the Russian standards.
</p>	' => '<p>Общество с ограниченной ответственностью «Рязцветмет» один из крупнейших в России поставщиков свинца и
                свинцовых
                сплавов. </p>

            <p>Начиная от простых видов сплавов, на данный момент компания поставляет широкий спектр современной
                продукции, отвечающей вызовам времени и соответствующей мировым стандартам. Признание высокого качества
                поставляемой продукции как на российском рынке, так и за рубежом, является лучшим показателем
                эффективности его работы.</p>',

    'Ltd «Ryazcvetmet»' => 'ООО "Рязцетмет"',

    'A wide range of certified products in all sizes' => 'Широкий ассортимент сертифицированной продукции в любых объемах',
    'Supply of high-quality refined lead, lead alloys and of calcium alloys' => 'Поставка высококачественного рафинированного свинца, свинцовых сплавов и кальциевых сплавов',
    'Purchase of scrap batteries' => 'Закупка лома аккумуляторных батарей',
    'Realization of both standard alloys and alloys under individual order' => 'Реализация как стандартных сплавов, так и сплавов под индивидуальный заказ',

    'The company has a laboratory equipped with modern technology, which allows
                     which allows to produce
                     products at the world level and fully compliant with Russian GOSTs.' => 'У компании имеется лаборатория, оснащенная современной техникой, что позволяет
                    что позволяет производить
                    продукцию на мировом уровне и полностью соответствующую Российским ГОСТам.',

    'Wide range of goods' => 'Широкий ассортимент',
    'Any tonnage' => 'Любые объемы',
    'Shortest period of production' => 'Кратчайшие сроки поставки',
    'All products are certified' => 'Вся продукция сертифицирована',

    'CONSULTATION' => 'КОНСУЛЬТАЦИЯ',

    'The managers of the company will gladly answer your questions and prepare an individual commercial offer.' => 'Менеджеры компании с радостью ответят на ваши вопросы и подготовят индивидуальное коммерческое предложение.',

    'Products' => 'Каталог товаров',
    'High-quality products' => 'Высококачественная продукция',

    'Lead' => 'Свинец',
    'High-quality refined lead and lead alloys. We can produce lead alloys and calcium alloys as per the customer’s requirement. All goods comply with ISO standards.'
    => 'Высококачественный рафинированный свинец марки С2С, свинцовые сплавы УС2, ССуА,ССуС. Под Ваш заказ изготавливаем любые свинцовые, кальциевые сплавы. Вся продукция соответствует мировым стандартам и ГОСТам России.',

    'Polypropylene (PP)' => 'Полипропилен',
    'Wide range of recycled polypropylene' => 'Вторичный полипропилен в ассортименте',

    'NEWS' => 'НОВОСТИ',
    'More' => 'Подробнее',

    'High-quality goods from the producer' => 'Высококачественная продукция от производителя',

    'High-quality refined lead and lead alloys. We can produce lead alloys and calcium alloys as per the customer’s requirement. All goods comply with ISO standards.' => 'Высококачественный рафинированный свинец марки С2С, свинцовые сплавы УС2, ССуА, ССуС. Под Ваш заказ изготавливаем любые свинцовые, кальциевые сплавы. Вся продукция соответствует мировым стандартам и ГОСТам России.',
    'Wide range of recycled polypropylene' => 'Вторичный полипропилен в ассортименте.',

    'Similar products' => 'Подобная продукция',




);