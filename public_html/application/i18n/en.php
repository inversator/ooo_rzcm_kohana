<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'user'                                                 => 'user',
    'name'                                                 => 'name',
    'phone'                                                => 'phone',
    'alias'                                                => 'alias',
    'username'                                             => 'username',
    'email'                                                => 'Email',
    'message'                                              => 'message',
    'pTitle'                                               => 'pTitle',
    'bTitle'                                               => 'bTitle',
    'password_confirm'                                     => 'Пpassword confirm',
    'password'                                             => 'password',
    'Previous'                                             => 'Previous',
    'First'                                                => 'First',
    'Next'                                                 => 'Next',
    'Last'                                                 => 'Last',

    'company' => 'company',
    'catalog' => 'catalog',
);
