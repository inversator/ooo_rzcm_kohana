<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Form extends Kohana_Form
{

    public static function pre($post)
    {

        $trueFields = array(
            'fixed', 'recommended'
        );

        $falseFields = array(
            'off'
        );

        if (isset($post['url'])) {
            $post['url'] = self::strToUrl($post['url']);
        }
        $textField = array('desc', 'text');

        foreach ($textField as $field) {
            if (array_key_exists($field, $post)) {
                //$post[$field] = strip_tags($post[$field]);
                $post[$field] = htmlspecialchars($post[$field]);
            }
        }


        foreach ($trueFields as $field) {
            if (array_key_exists($field, $post)) {
                $post[$field] = 1;
            } else {
                $post[$field] = 0;
            }
        }

        foreach ($falseFields as $field) {
            if (isset($post[$field]) && $post[$field] == 'true') {
                $post[$field] = 0;
            } else {
                $post[$field] = 1;
            }
        }



        return $post;
    }

    public static function rusTranslit($string)
    {

        $converter = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        );

        return strtr($string, $converter);
    }

    public static function strToUrl($str)
    {

        // переводим в транслит
        $str = self::rusTranslit($str);

        // в нижний регистр
        $str = strtolower($str);

        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);

        // удаляем начальные и конечные '-'
        $str = trim($str, "-");

        return $str;
    }
}