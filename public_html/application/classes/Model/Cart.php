<?php

//Room::syspath();

class Model_Cart extends Model_Base
{

    function addToCart($id, $count = 1)
    {
        if (!empty($_SESSION['cart'][$id])) {
            // Увеличиваем количество если товар уже добавлен:
            $_SESSION['cart'][$id]['count'] += $count ? $count : 1;
        } else {
            // Создаем пустой массив
            $_SESSION['cart'][$id] = array();

            // Извлекаем цену
            $price = Model::factory('unit')->getPrice($id);

            // Добавляем товар в корзину:
            $_SESSION['cart'][$id]['coast'] = $price;
            $_SESSION['cart'][$id]['count'] = $count ? $count : 1;
        }
    }

    function getCart()
    {
        if (session::instance()->get('cart')) {
            $units = array_keys(session::instance()->get('cart'));
        } else {
            $units = array(0);
        }
        return DB::select('u.id', 'u.pTitle', 'u.url', 'p.value')
                ->from(array('units', 'u'))
                ->join(array('property_values', 'p'))
                ->on('u.id', '=', 'p.id_unit')
                ->where('p.id_prop', '=', 1)
                ->and_where('u.id', 'in', $units)
                ->execute();
    }

    function delFromCart($id, $count = 1)
    {
        unset($_SESSION['cart'][$id]);
    }

    function clearCart()
    {
        unset($_SESSION['cart']);
    }

    function changeCount($id, $sign)
    {
        $_SESSION['cart'][$id]['count'] += $sign;
        if (!$_SESSION['cart'][$id]['count']) unset($_SESSION['cart'][$id]);
    }
}