<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Category extends Model_Base
{
    protected $_table_name = 'categories';
    protected $_table_columns = array(
        'id' => NULL,
        'meta_k' => NULL,
        'meta_d' => NULL,
        'bTitle_ru' => NULL,
        'bTitle_en' => NULL,
        'pTitle_ru' => NULL,
        'pTitle_en' => NULL,
        'desc_ru' => NULL,
        'desc_en' => NULL,
        'text_ru' => NULL,
        'text_en' => NULL,
        'off' => NULL,
        'url' => NULL,
    );

    protected function rules()
    {

        return array(
            'pTitle_ru' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'pTitle_en' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'bTitle_ru' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'bTitle_en' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'meta_k' => array(
                array('max_length', array(':value', 255)),
            ),
            'meta_d' => array(
                array('max_length', array(':value', 255)),
            ),
            'url' => array(
                array('not_empty'),
                array('max_length', array(':value', 25)),
                array('unic_field', array(':value', ':field', 'category', parent::$specimen['id']))
            ),
        );
    }

    public function getList()
    {

        $sql = "SELECT c.id, c.meta_k, c.meta_d, c.desc_" . I18n::lang() . ", c.text_" . I18n::lang() . ", c.url, c.bTitle_" . I18n::lang() . ", c.pTitle_" . I18n::lang() . "
FROM `categories` AS `c` WHERE `c`.`id` NOT IN (Select dot_record FROM relation WHERE dot_essence = 'category') AND c.off = 0
GROUP BY `c`.`id`";

        $parRecords = DB::query(1, $sql)->execute()->as_array();

        for ($i = 0; $i < count($parRecords); $i++) {

            $parRecords[$i]['dot_records'] = DB::select('c.id', 'c.meta_k',
                'c.meta_d', 'c.desc_' . I18n::lang(), 'c.text_' . I18n::lang(), 'c.url', 'c.bTitle_' . I18n::lang(),
                'c.pTitle_' . I18n::lang())
                ->from(array('categories', 'c'))
                ->join(array('relation', 'r'))
                ->on('c.id', '=', 'r.dot_record')
                ->where('r.par_essence', '=', 'category')
                ->and_where('dot_essence', '=', 'category')
                ->and_where('par_record', '=', $parRecords[$i]['id'])
                ->and_where('off','=',0)
                ->execute()
                ->as_array();
        }
        return $parRecords;
    }

    public function getCatalog()
    {
        $sql = "SELECT c.id, c.meta_k, c.meta_d, c.desc_" . I18n::lang() .", c.text_" . I18n::lang() .", c.url, c.bTitle_" . I18n::lang() .", c.pTitle_" . I18n::lang() ."
FROM `categories` AS `c` WHERE `c`.`id` IN (Select dot_record FROM relation WHERE dot_essence = 'category' AND par_record = 40) AND off =0
GROUP BY `c`.`id`";

        $parRecords = DB::query(1, $sql)->execute()->as_array();

        for ($i = 0; $i < count($parRecords); $i++) {

            $parRecords[$i]['dot_records'] = DB::select('c.id', 'c.meta_k',
                'c.meta_d', 'c.desc_' . I18n::lang(), 'c.text_'  . I18n::lang(), 'c.url', 'c.bTitle_'  . I18n::lang(),
                'c.pTitle_'  . I18n::lang())
                ->from(array('units', 'c'))
                ->join(array('relation', 'r'))
                ->on('c.id', '=', 'r.dot_record')
                ->where('r.par_essence', '=', 'category')
                ->and_where('dot_essence', '=', 'unit')
                ->and_where('c.lang', '=', I18n::lang())
               ->and_where('par_record', '=', $parRecords[$i]['id'])
                ->and_where('off','=',0)
                ->execute()
                ->as_array();
        }

        return $parRecords;
    }

    public function getSub($id)
    {

        $subRecords = DB::select('c.id', 'c.meta_k',
            'c.meta_d', 'c.desc_'  . I18n::lang(), 'c.text_'  . I18n::lang(), 'c.url', 'c.bTitle_'  . I18n::lang(),
            'c.pTitle_'  . I18n::lang())
            ->from(array('categories', 'c'))
            ->join(array('relation', 'r'))
            ->on('c.id', '=', 'r.dot_record')
            ->where('r.par_essence', '=', 'category')
            ->and_where('dot_essence', '=', 'category')
            ->and_where('par_record', '=', $id)
            ->execute()
            ->as_array();

        return $subRecords;
    }

    // Получаем список фильтров
    public function getFilters($id)
    {
        $sql = "
                SELECT p.alias, p.name, p.id, GROUP_CONCAT(pv.value) as `values`
                FROM properties p 
                JOIN property_values pv
                ON pv.id_prop = p.id
                WHERE pv.id_unit IN
                (
                    SELECT 
                    u.id

                    FROM units u
                    JOIN relation r 

                    ON r.par_essence = 'category' 

                    AND r.dot_essence = 'unit' 
                    AND r.par_record = :id
                    AND r.dot_record = u.id

                    WHERE u.off = 0
                )
                GROUP BY p.id
                ";

        return DB::query(1, $sql)->param(':id', $id)->execute();
    }

    public function filters($filters, $id_cat, $limit, $offset, $sort = 't.id')
    {
        $filtersList = ' AND u.id IN (
                        SELECT p.id_unit 
                        FROM property_values p     
                        WHERE';
        $fiest = 1;
        $countProp = 0;

        foreach ($filters as $prop => $values) {
            // Это счетчик количества свойств (он считает именно свойства, а не количество их значений)
            $countProp++;

            if (!$fiest) {
                $filtersList .= ' OR p.id_prop = ' . $prop . ' AND (';
            } else {
                $filtersList .= ' p.id_prop = ' . $prop . ' AND (';
            }

            foreach ($values as $value) {
                $filtersList .= ' p.value = "' . $value . '" OR';
            }

            $filtersList = substr($filtersList, 0, -2);

            $filtersList .= ' ) ';

            $fiest = 0;
        }

        $filtersList .= 'GROUP BY(p.id_unit) HAVING COUNT(p.id_unit) = ' . $countProp . ') ';

        // Получаем видимые товары текущей категории
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.*,
                    i.name as image,
                    p.value as price

                    FROM units u
                    JOIN relation r 

                    ON r.par_essence = 'category' 

                    AND r.dot_essence = 'unit' 
                    AND r.par_record = :id_cat
                    AND r.dot_record = u.id

                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1
                    
					
                    WHERE u.off = 0
                    $filtersList
                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
                ORDER BY $sort
                LIMIT $limit
                OFFSET $offset
                
               ";

        return (DB::query(1, $sql)->param(':id_cat', $id_cat)->execute());
    }

    public function takeUnits($id)
    {

        // Получаем видимые товары текущей категории
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.*,
                    i.name as image,
                    p.value as price

                    FROM units u
                    JOIN relation r 

                    ON r.par_essence = 'category' 

                    AND r.dot_essence = 'unit' 
                    AND r.par_record = :id
                    AND r.dot_record = u.id

                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1

                    WHERE u.off = 0 AND u.lang = '" . I18n::lang() . "'

                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
                ORDER by t.date DESC
               ";

        return DB::query(1, $sql)->param(':id', $id)->execute();
    }

    public function takeUnitsPage($id, $limit, $offset, $sort = 't.id')
    {

        // Получаем видимые товары текущей категории
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.*,
                    i.name as image,
                    p.value as price

                    FROM units u
                    JOIN relation r 

                    ON r.par_essence = 'category' 

                    AND r.dot_essence = 'unit' 
                    AND r.par_record = :id
                    AND r.dot_record = u.id

                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1

                    WHERE u.off = 0

                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
                ORDER BY t.date DESC 
                LIMIT $limit 
                OFFSET $offset
               ";
        //mysql_query($sql) or die(mysql_error());
        return (DB::query(1, $sql)->param(':id', $id)->execute());
    }

    public function takeAllUnitsPage($limit, $offset = 0)
    {
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.*,
                    i.name as image,
                    p.value as price

                    FROM units u
                    JOIN relation r 

                    ON r.par_essence = 'category' 

                    AND r.dot_essence = 'unit' 
                    AND r.dot_record = u.id

                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1

                    WHERE u.off = 0

                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
                LIMIT $limit 
                OFFSET $offset
               ";
        //mysql_query($sql) or die(mysql_error());
        return DB::query(1, $sql)->execute();
    }

    public function countSelected($filters, $id_cat)
    {
        $filtersList = ' AND u.id IN (
                        SELECT p.id_unit 
                        FROM property_values p     
                        WHERE';
        $fiest = 1;
        $countProp = 0;

        foreach ($filters as $prop => $values) {
            // Это счетчик количества свойств (он считает именно свойства, а не количество их значений)
            $countProp++;

            if (!$fiest) {
                $filtersList .= ' OR p.id_prop = ' . $prop . ' AND (';
            } else {
                $filtersList .= ' p.id_prop = ' . $prop . ' AND (';
            }

            foreach ($values as $value) {
                $filtersList .= ' p.value = "' . $value . '" OR';
            }

            $filtersList = substr($filtersList, 0, -2);

            $filtersList .= ' ) ';

            $fiest = 0;
        }

        $filtersList .= 'GROUP BY(p.id_unit) HAVING COUNT(p.id_unit) = ' . $countProp . ') ';

        // Получаем количество выбранных товаров
        $sql = "
                SELECT COUNT(*) as count FROM
                (
                    SELECT u.id

                    FROM units u
                    JOIN relation r 

                    ON r.par_essence = 'category' 

                    AND r.dot_essence = 'unit' 
                    AND r.par_record = :id_cat
                    AND r.dot_record = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    					
                    WHERE u.off = 0
                    $filtersList
                    GROUP BY u.id
                ) t
                
               ";
        $result = DB::query(1, $sql)->param(':id_cat', $id_cat)->execute();

        return $result[0]['count'];
    }

    public function countProducts($id)
    {

//        $catUnits = DB::select(array(DB::expr('COUNT(*)'), 'count'))
//            ->from(array('units', 'u'))
//            ->join(array('relation', 'r'))
//            ->on('u.id', '=', 'dot_record')
//            ->where('r.par_essence', '=', 'category')
//            ->where('r.dot_essence', '=', 'unit')
//            ->and_where('r.par_record', '=', $id)
//            ->and_where('u.off', '<>', 1)
//            ->group_by('u.id')
//            ->execute()
//            //->as_array()
//            ;

        $sql = "SELECT COUNT(*) "
            . "as count FROM ("
            . "SELECT u.id AS `count` FROM `units` AS `u` "
            . "JOIN `relation` AS `r` "
            . "ON (`u`.`id` = `dot_record`) "
            . "WHERE `r`.`par_essence` = 'category' "
            . "AND `r`.`dot_essence` = 'unit' "
            . "AND `r`.`par_record` = :id "
            . "AND `u`.`off` <> 1 "
            . "GROUP BY `u`.`id`) as t";

        $catUnits = DB::query(1, $sql)->param(':id', $id)->execute()->as_array();

        return $catUnits[0]['count'];
    }
}