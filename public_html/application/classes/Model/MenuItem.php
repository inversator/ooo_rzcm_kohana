<?php

class Model_MenuItem extends Model_Base
{
    protected $_table_name = 'menu_item';
    protected $_table_columns = array(
        'id' => NULL,
        'name' => NULL,
        'alias' => NULL,
        'off' => NULL,
        'type' => NULL,
        'position' => NULL,
        'par_item' => NULL,
    );

    public function getActiveItems($type)
    {
        $sql = "SELECT * FROM ".$this->_table_name." WHERE off <> '1' AND type =:type ORDER BY position";

        return DB::query(1, $sql)->param(':type', $type)->execute()->as_array();
    }

    public function getAll($type = NULL)
    {
        $sql = "SELECT * FROM ".$this->_table_name." WHERE type =:type";

        return DB::query(1, $sql)->param(':type', $type)->execute();
    }
    
    public function getSubItem($alias)
    {
        $sql = "SELECT p.pTitle_" . I18n::lang() . " as title, p.url FROM pages p "
            . "JOIN relation r ON r.dot_record = p.id "
            . "WHERE r.par_record = (SELECT id FROM pages WHERE url = :alias) "
            . "AND r.par_essence = 'page' "
            . "AND r.dot_essence = 'page'";
        return DB::query(1, $sql)->param(':alias',$alias)->execute();
    }

    public function getSubItems($id)
    {
        return DB::select('id','name','alias','position','off')
            ->from('menu_item')
            ->where('par_item','=',$id)
            //->as_object()
            ->execute()
            ->as_array()
            ;
    }
}