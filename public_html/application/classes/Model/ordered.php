<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Ordered extends Model_Base
{
    protected $_table_name = 'ordered_products';
    protected $_essence = 'ordered_products';
    protected $_table_columns = array(
        'id' => NULL,
        'id_prod' => NULL,
        'id_order' => NULL,
        'count' => NULL
    );

}