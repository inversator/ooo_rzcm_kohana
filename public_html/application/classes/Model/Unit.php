<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Model_Unit extends Model_Base
{
    protected $_table_name = 'units'; //Если имя иное
    //protected $_primary_key= 'id';
    protected $_table_columns = array(
        'id' => NULL,
        'meta_k' => NULL,
        'meta_d' => NULL,
        'bTitle_ru' => NULL,
        'bTitle_en' => NULL,
        'pTitle_ru' => NULL,
        'pTitle_en' => NULL,
        'desc_ru' => NULL,
        'desc_en' => NULL,
        'text_ru' => NULL,
        'text_en' => NULL,
        'off' => NULL,
        'url' => NULL,
        'date' => NULL,
        'lang' => NULL,
        'recommended' => NULL,
    );

    public function getProperties($id_unit)
    {
        $v = 'property_values';
        $p = 'properties';

        return DB::select($v.'.value', $v.'.pos', $v.'.id', $p.'.alias',
                    $v.'.id_prop')
                ->from($v)
                ->join($p)
                ->on($v.'.id_prop', '=', $p.'.id')
                ->where($v.'.id_unit', '=', $id_unit)
                ->order_by($v.'.pos')
                ->execute()
        ;
    }

    public function getRecommended()
    {
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.*,
                    i.name as image,
                    p.value as price

                    FROM units u
                    
                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1

                    WHERE u.off = 0
                    AND u.recommended = 1
                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
               ";
        return DB::query(1, $sql)->execute();
    }

    public function getLast()
    {
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.*,
                    i.name as image,
                    p.value as price

                    FROM units u
                    
                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1

                    WHERE u.off = 0

                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
                ORDER BY t.date DESC
                LIMIT 9
               ";
        return DB::query(1, $sql)->execute();
    }

    public function relationUnits($id)
    {
        $sql = "
                SELECT t.* FROM
                (
                    SELECT u.*,
                    i.name as image,
                    p.value as price

                    FROM units u
                    JOIN relation r 

                    ON r.par_essence = 'unit' 

                    AND r.dot_essence = 'unit' 
                    AND r.par_record = u.id
                    AND r.dot_record = :id

                    LEFT JOIN images i 
                    ON i.essence = 'unit' 
                    AND i.id_es = u.id
                    
                    LEFT JOIN property_values p
                    ON p.id_unit = u.id 
                    AND p.id_prop = 1

                    WHERE u.off = 0

                    ORDER BY i.main DESC
                ) t
                GROUP BY t.id
               ";
        //mysql_query($sql) or die(mysql_error());
        return DB::query(1, $sql)->param(':id', $id)->execute();
    }

    public function getImages($id)
    {
        return DB::select('i.name', 'i.main')
                ->from(array('images', 'i'))
                ->where('id_es', '=', $id)
                ->and_where('essence', '=', 'unit')
                ->execute();
    }

    public function getPrice($id)
    {
        $result = DB::select('value')
            ->from('property_values')
            ->where('id_prop', '=', '1')
            ->and_where('id_unit', '=', $id)
            ->execute()
            ->as_array();

        return isset($result[0]['value']) ? $result[0]['value'] : 0;
    }

    protected function rules()
    {
        return array(
            'pTitle_ru' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'pTitle_en' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'bTitle_ru' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),
            'bTitle_en' => array(
                array('not_empty'),
                array('max_length', array(':value', 255)),
            ),

            'meta_k' => array(
                array('max_length', array(':value', 255)),
            ),
            'meta_d' => array(
                array('max_length', array(':value', 255)),
            ),
            'url' => array(
                array('not_empty'),
                array('max_length', array(':value', 100)),
                array('unic_field', array(':value', ':field', 'unit', parent::$specimen['id']))
            ),
        );
    }
}