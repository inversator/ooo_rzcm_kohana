<?php
class HTTP_Exception_404 extends Kohana_HTTP_Exception_404 {

    public function get_response()
    {
        $response = Response::factory()
            ->status(404)
            ->body(Request::factory('/404')->execute());
        
        return $response;
    }
}