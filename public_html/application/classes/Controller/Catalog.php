<?php

/**
 * Created by PhpStorm.
 * User: maa
 * Date: 27.01.2017
 * Time: 16:29
 */
class Controller_Catalog extends Controller_Base
{

    public function action_index()
    {

        $catProduct = $this->request->param('catProduct');
        $modelCat = Model::factory('Category');

        if (empty($catProduct)) {

            $category = $modelCat->getOnUrl('catalog');

            $content = View::factory('page/catalog');
            $content->self = $category;

        } else {
            $category = $modelCat->getOnUrl($catProduct);

            if (!empty($product = $this->request->param('product'))) {

                $content = View::factory('page/product');
                $content->category = $category;
                $unit = Model::factory('Unit')->getOnUrl($product);
                $content->self = $unit;
                $images = Model::factory('Unit')->getImages($unit['id']);
                $content->images = $images;

                // Вынимаем главное фото
                $mainImage = NULL;
                foreach ($images->as_array() as $image) {
                    if ($image['main']) $mainImage = $image['name'];
                }
                $content->mainImage = $mainImage ? $mainImage : $images[0]['name'];

                // Получаем связанные товары
                $units = Model::factory('Unit')->relationUnits($unit['id']);
                $content->units = $units->as_array();

                $content->category = $category;
                $content->modal = View::factory('modal/questionProd');

                $content->rigthMenu = view::factory('block/prod_menu');

                // Получаем список категорий
                $categories = Model::factory('category')->getCatalog();
                $content->rigthMenu->list = $categories;


            } else {
                $units = $modelCat->takeUnits($category['id']);

                $content = View::factory(('page/catalog_prod'));
                $content->self = $category;
                $content->units = $units->as_array('id');
            }


        }

        $this->template->content = $content;
    }

}
