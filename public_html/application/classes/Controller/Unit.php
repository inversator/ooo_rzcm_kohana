<?php

class Controller_Unit extends Controller_Base
{
    public $alias = 'Товар';
    public $essence = 'unit';
    public $gender = 'male';
    public $model = 'unit';

    public function action_index()
    {
        $page = Model::factory($this->model)->get(1);
        $content = View::factory($this->essence . '/info');

        $content->title = $page['pTitle_' . I18n::lang()];
        $content->desc = $page['desc_' . I18n::lang()];
        $content->text = $page['text_' . I18n::lang()];
        $content->date = $page['date_' . I18n::lang()];

        $this->template->content = $content;
        $this->template->bTitle = $page['bTitle_' . I18n::lang()];
        $this->template->content->rigthMenu = view::factory('block/rigth_menu');

        // Получаем список категорий 
        $categories = Model::factory('category')->getList();
        $this->template->content->rigthMenu->list = $categories;
    }

    public function action_item()
    {
        if (!$slug = $this->request->param('unit')) {
            if (!$slug = $this->request->param('slug')) {
                $slug = $this->request->param('slug2');
            }
        }

        $page = Model::factory($this->model)->getOnUrl($slug);

        $content = View::factory($this->essence . '/info');

        $content->title = $page['pTitle_' . I18n::lang()];
        $content->desc = $page['desc_' . I18n::lang()];
        $content->text = $page['text_' . I18n::lang()];
        $content->date = $page['date'];
        $content->id = $page['id'];

        $this->template->content = $content;
        $this->template->bTitle = $page['bTitle_' . I18n::lang() ];
        $this->template->content->rigthMenu = view::factory('block/rigth_menu');

        // Получаем список категорий (для меню)
        $categories = Model::factory('category')->getList();
        $this->template->content->rigthMenu->list = $categories;

        // Получаем список свойств товара
        $properties = Model::factory($this->model)->getProperties($page['id']);
        $properties = $properties->as_array();

        // Вынимаем и удаляем цену
        $price = 0;

        for ($i = 0; $i < count($properties); $i++) {
            if ($properties[$i]['id_prop'] == 1) {
                $price = $properties[$i]['value'];
                unset($properties[$i]);
            }
        }
        $this->template->content->price = $price;
        $this->template->content->properties = $properties;

        // Изображения
        $images = Model::factory($this->model)->getImages($page['id']);
        $this->template->content->images = $images->as_array();

        // Вынимаем главное фото
        $mainImage = NULL;

        foreach ($images->as_array() as $image) {
            if ($image['main']) $mainImage = $image['name'];
        }
        $this->template->content->mainImage = $mainImage ? $mainImage : $images[0]['name'];

        // Получаем связанные товары
        $units = Model::factory($this->model)->relationUnits($page['id']);
        $this->template->content->units = $units->as_array();
    }
}