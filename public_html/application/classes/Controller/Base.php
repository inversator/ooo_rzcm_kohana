<?php
defined('SYSPATH') or die('Прямой доступ запрещен.');

abstract class Controller_Base extends Controller_Template
{
    public $template = 'main';
    public $bTitle = '';

    public function before()
    {
        parent::before();

        $footer = View::factory('block/footer');
        $url = URL::site(NULL, TRUE);

        // Todo quick edit
        preg_match('/ru|en/', $url, $matches);
        if ((count($matches)))
            I18n::lang($matches[0]);
        else
            I18n::lang('ru');

        $this->template->content = '';
        $this->template->styles = array('main');
        $this->template->scripts = '';
        $this->template->footer = $footer;

        $type = 4;

        $menuUrl = '/menu/get/' . $type . "?controller=" . $this->request->param('slug');
        $menu = Request::factory($menuUrl)->execute();
        $this->template->navigation = $menu;
        $this->template->header = View::factory('block/header');
    }

    public function after()
    {
        parent::after();
    }
}