<?php

class Controller_Page extends Controller_Base
{

    public function action_index()
    {

        $page = Model::factory('Page')->get(1);
        $content = View::factory('face');

        $content->title = $page['pTitle_' . I18n::lang()];
        $content->desc = $page['desc_' . I18n::lang()];
        $content->text = $page['text_' . I18n::lang()];

        $this->template->content = $content;
        $this->template->bTitle = $page['bTitle_' . I18n::lang()];

        // Выводим новости
        $news = Model::factory('category')->takeUnits(39);
        $this->template->content->newsList = $news->as_array();
    }

    public function action_item()
    {

        if($this->request->param('slug2')) {
            $slug = $this->request->param('slug2');
        } else {
            $slug = $this->request->param('slug');
        }

        if ($slug == 'main') $this->redirect('/');
        $page = Model::factory('Page')->getOnUrl($slug);

        $content = View::factory('page');

        $content->title = $page['pTitle_' . I18n::lang()];
        $content->desc = htmlspecialchars_decode($page['desc_' . I18n::lang()]);
        $content->text = htmlspecialchars_decode($page['text_' . I18n::lang()]);
        $this->template->content = $content;
        $this->template->bTitle = $page['bTitle_' . I18n::lang()];
        
    }

    public function action_vacancy()
    {

        $modelCat = Model::factory('Category');
        $category = $modelCat->getOnUrl('vacancy');
        $units = $modelCat->takeUnits($category['id']);

        $content = View::factory('page/vacancy');
        $content->self = $category;
        $content->modal = View::factory('modal/resume');

        $content->vacancys = $units->as_array();
        $this->template->content = $content;
    }

    public function action_404()
    {

    }
}