<?php
defined('SYSPATH') or die('Прямой доступ зарпещен');

class Controller_Articles extends Controller_Base
{

    public function action_index()
    {
        $id = $this->request->param('id');
        
        if($id)
        {
            $content = View::factory('article');
            $content->article = $id;
        }
        else
        {
            $content = View::factory('articles');
        }
        $this->template->content = $content;
        $this->template->bTitle = 'Статьи';
    }
}