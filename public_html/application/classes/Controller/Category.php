<?php

class Controller_Category extends Controller_Base
{
    public $alias = 'Категория';
    public $essence = 'category';
    public $gender = 'female';
    public $model = 'Category';

    public function action_index()
    {
        $this->redirect('/');

        $content = View::factory($this->essence);

        $content->title = 'Товары';
        $content->desc = 'Товары';
        $content->text = 'Товары';
        $content->url = '';

        $this->template->content = $content;
        $this->template->bTitle = 'Товары и категории';
        $this->template->content->rigthMenu = view::factory('block/rigth_menu');

        // Получаем список категорий 
        $categories = Model::factory($this->model)->getList();
        $this->template->content->rigthMenu->list = $categories;

        $count = Model::factory('Unit')->count();

        $pagination = Pagination::factory(array(
                'total_items' => $count,
                'items_per_page' => 10
                )
            )->route_params(array(
            'controller' => 'category',
        ));

        //Получаем список всех товаров
        $units = Model::factory($this->model)
            ->takeAllUnitsPage($pagination->items_per_page, $pagination->offset);

        $this->template->content->units = $units;
        $this->template->content->pagination = $pagination;
    }

    public function action_item()
    {


        if (!$slug = $this->request->param('slug2')) {
            $slug = $this->request->param('slug');
        }

        $page = Model::factory($this->model)->getOnUrl($slug);

        $content = View::factory('info/'.$this->essence);

        $content->title = $page['pTitle_' . I18n::lang()];
        $content->desc = $page['desc_' . I18n::lang()];
        $content->text = $page['text_' . I18n::lang()];
        $content->url = $page['url'];
        $content->id = $page['id'];

        $this->template->content = $content;
        $this->template->bTitle = $page['bTitle_' . I18n::lang()];
        $this->template->content->rigthMenu = view::factory('block/rigth_menu');

        // Получаем список категорий 
        $categories = Model::factory($this->model)->getList();
        $this->template->content->rigthMenu->list = $categories;

        $count = Model::factory($this->model)->countProducts($page['id']);

        $asincPag = new asincPag;
        $pagination = $asincPag->create($count, $this->request->query('page'), 10);

        // Получаем список товаров категории
        $units = Model::factory($this->model)
            ->takeUnitsPage(
            $page['id'], $asincPag->itemsPerPage, $asincPag->offset);

        $this->template->content->units = $units;
        $this->template->content->pagination = $pagination;
        $this->template->content->slug = $slug;

        // Получаем список фильтров
        $filters = Model::factory('Category')->getFilters($page['id']);
        $this->template->content->filters = $filters->as_array();
    }

    public function action_filters()
    {
        
        $filters = $this->request->post('filters');
        $cat_id = $this->request->post('cat');
        $slug = $this->request->post('slug');
        $page = $this->request->post('page');

        $sort = str_replace('%20', ' ', $this->request->post('sort'));

        if (count($filters) > 0) {
            $count = Model::factory($this->model)->countSelected($filters,
                $cat_id);
        } else {
            $count = Model::factory($this->model)->countProducts($cat_id);
        }

        $asincPag = new asincPag;
        
        $pagination = $asincPag->create($count, $page, 10);
//        Room::die_dump($pagination);
        // Получаем список товаров категории

        if (count($filters) > 0) {
            $units = Model::factory($this->model)
                ->filters(
                $filters, $cat_id, $asincPag->itemsPerPage, $asincPag->offset,
                $sort
            );
        } else {
            $units = Model::factory($this->model)
                ->takeUnitsPage(
                $cat_id, $asincPag->itemsPerPage, $asincPag->offset, $sort
            );
        }

        $view = View::factory('pager');

        $view->units = $units->as_array();
        $view->url = $slug;
        
        $arr['units'] = $view->render();
        $arr['pagination'] = $pagination;
        //Room::die_dump($arr);
        $this->response->body(json_encode($arr));

        $this->auto_render = FALSE;
    }
}