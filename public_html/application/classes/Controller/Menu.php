<?php

class Controller_Menu extends Controller
{

    public function action_get()
    {
        $type = $this->request->param('id');

        $model = new Model_MenuItem();
        $items = $model->getActiveItems($type);

        $menu = View::factory('block/navigation');
        $menu->lis = '';

        foreach ($items as $item) {
            $subItem = $model->getSubItem($item['name'])->as_array();

            if (count($subItem) == 0) {
                $menu->lis .= Menu::liMark($item['name'], $item['alias']);
            } else {
                
                $menu->lis .= Menu::liDropMark($item['name'], $item['alias'], $subItem);
            }
        }

        $this->response->body($menu);
    }
}