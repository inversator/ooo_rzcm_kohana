<?php

class Controller_Order extends Controller_Base
{
    public $alias = 'Оформление';
    public $essence = 'order';
    public $gender = 'male';
    public $model = 'Order';

    public function action_index()
    {
        $content = View::factory($this->essence);

        $content->title = 'Оформление заказа';

        $sessionCart = session::instance()->get('cart');

        $cartBlock = Model::factory('cart')->getCart()->as_array('id');

        // Объединяю сессионные данные с данными модели
        $sum = 0;

        if (count($cartBlock)) {
            foreach ($cartBlock as $unit) {
                $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
            }

            foreach ($cartBlock as $unit) {
                $sum += $unit['value'] * $unit['count'];
            }
        }

        $content->cart = $cartBlock;
        $content->sum = $sum;

        $this->template->content = $content;
        $this->template->bTitle = $content->title;
        $this->template->content->rigthMenu = view::factory('block/rigth_menu');

        // Получаем список категорий 
        $categories = Model::factory('category')->getList();
        $this->template->content->rigthMenu->list = $categories;

    }

    public function action_make()
    {
        $this->auto_render = FALSE;

        $model = Model::factory($this->model);
        $post = $model->validation($this->request->post());

        if ($post->check()) {
            if ($model->save($this->request->post())) {
                $orderId = $model->lastID();

                // Перетряхиваем корзину
                $sessionCart = session::instance()->get('cart');

                $cartBlock = Model::factory('cart')->getCart()->as_array('id');

                // Объединяю сессионные данные с данными модели
                $sum = 0;

                if (count($cartBlock)) {
                    foreach ($cartBlock as $unit) {
                        $cartBlock[$unit['id']]['count'] = $sessionCart[$unit['id']]['count'];
                    }

                    foreach ($cartBlock as $unit) {
                        $sum += $unit['value'] * $unit['count'];
                    }
                }
                
                $block = "<table class='table'>";

                foreach ($cartBlock as $unit) {

                    $block .= '<tr>'
                             .'<td>'.$unit['pTitle'].'<span> x '.$unit['count'].'</span></td>'
                             .'<td>'.$unit['value'] * $unit['count'].'</td>'
                             .'</tr>';
                    
                    // Заносим данные о заказанных товарах в таблицу
                    Model::factory('Ordered')->save(array('id' => 0, 'id_prod' => $unit['id'], 'id_order' => $orderId, 'count' => $unit['count']));                    
                }

                $block .= "<tr><td>Итого</td><td>$sum</td></tr>";
                $block .= "</table>";

                // Отправляем сообщение
                $config = KOHANA::$config->load('email');
                Email::connect($config);

                $view = View::factory('mail/order');
                $view->post = $this->request->post();
                $view->block = $block;

                $to = Kohana::$config->load('main.email');

                $subject = "Заказ #".$orderId." на сайте L'image";
                $from = array('manager@limage62.ru','LIMAGE');

                $sendMessage = $view;
                Email::send($to, $from, $subject, $sendMessage, $html = true);
                
                $message['result'] = 1;
            } else {
                $message['result'] = 0;
            };
        } else {
            $message['result'] = 0;
            $message['error'] = $post->errors();
        }

        $this->response->body(json_encode($message));
    }

    public function action_check()
    {
        $this->auto_render = FALSE;

        $model = Model::factory($this->model);
        $post = $model->validation($this->request->post());

        if ($post->check()) {
            $message['result'] = 0;
            $message['error'] = 0;
        } else {
            $message['result'] = 0;
            $message['error'] = $post->errors();
        }

        $this->response->body(json_encode($message));
    }
}