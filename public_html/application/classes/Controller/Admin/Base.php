<?php
defined('SYSPATH') or die('Прямой доступ запрещен.');

abstract class Controller_Admin_Base extends Controller_Template
{
    public $template = 'admin/main';
    protected $genus = '';
    public $menu = '';
    public $pTitle = '';
    public $bTitle = '';
    public $alias = '';
    protected $essence = '';
    protected $model_name = '';

    public function before()
    {
        parent::before();

        if (!Auth::instance()->logged_in('admin'))
                $this->redirect('/admin/login');
        else $user = Auth::instance()->get_user();

        $footer = View::factory('admin/blocks/footer');
        $nav = View::factory('admin//blocks/navigation');

        $nav->user = $user;

        $this->template->content = '';
        $this->template->styles = array('main');
        $this->template->scripts = '';
        $this->template->footer = $footer;
        $this->template->navigation = $nav;
    }

    public function action_delete()
    {
        $this->auto_render = FALSE;
        $message = '';

        if (!count($_POST)) {
            $message = 'Данные не поступили';
        } else {
            $model = Model::factory($this->model_name);

            if ($model->delete(Arr::get($_POST, 'id', 0))) {
                $message = $this->essence.' удален'.KOHANA::$config->load('converter.genus.'.$this->genus);
            } else {
                $message = 'Объект не найден';
            }
        }
        $this->response->body($message);
    }

    public function action_new()
    {

        $content = View::factory('admin/'.$this->alias.'/edit');

        $content->pTitle = 'Создание';
        // Метка действия для скрипты
        $content->act = 'new';
        $content->page = array();

        $this->template->content = $content;
        $this->template->menu = $this->menu;
    }

    public function action_save()
    {

        $this->auto_render = FALSE;

        $message = '';

        if (!isset($_POST['id'])) {
            $message = 'Отсутствует идентификатор';
        } else {

            $request = Form::pre($_POST);

            $model = Model_Base::factory($this->alias, $request['id']);

            $post = $model->validation($request);

            if ($post->check()) {

                if ($model->save($request)) {
                    if (!empty($request['id'])) {
                        $message = 'Изменения сохранены';
                    } else {
                        $message = $this->essence.' добавлен'.KOHANA::$config->load('converter.genus.'.$this->genus).'. ';
                        $message .= '<a href="/admin/'.$this->alias.'/edit/'.$model->lastId().'">Перейти к редактированию -></a>';
                    }
                } else {
                    $message = 'Вы не изменили данные';
                };
            } else {
                foreach ($post->errors('comments') as $error) {
                    $message .= "<p>".$error."</p>";
                }
            }
        }

        $this->response->body($message);
    }

    public function action_edit()
    {
        $id = $this->request->param('id');

        if (!$id) {
            $this->redirect('/admin/'.$this->alias.'/');
        }

        $content = View::factory('admin/'.$this->alias.'/edit');

        $content->pTitle = 'Редактирование';

        $page = array();
        $model = Model::factory($this->model_name);

        $page = $model->get($id);
        $content->page = $page;

        // Метка действия для скрипты
        $content->act = 'edit';

        $this->template->content = $content;
        $this->template->menu = $this->menu;
    }

    public function action_view()
    {
        $id = $this->request->param('id');

        if (!$id) {
            $this->redirect('/admin/'.$this->alias.'/');
        }

        $content = View::factory('admin/'.$this->alias.'/view');

        $content->pTitle = 'Просмотр';

        $page = array();
        $model = Model::factory($this->model_name);

        $page = $model->get($id);
        $content->page = $page;

        // Метка действия для скрипта
        $content->act = 'view';

        $this->template->content = $content;
        $this->template->menu = $this->menu;
    }

    public function action_index()
    {
        $pages = array();

        $model = Model::factory($this->model_name);

        $count = $model->count();

        $pagination = Pagination::factory(array(
                'total_items' => $count,
                )
            )->route_params(array(
            'directory' => strtolower($this->request->directory()),
            'controller' => strtolower($this->request->controller()),
            'action' => 'list',
        ));

        $pages = $model->getAllLim(
            $pagination->offset, $pagination->items_per_page);
        
        $content = View::factory('admin/'.$this->alias.'/list');

        $content->pTitle = $this->pTitle;
        $content->pages = $pages;
        $content->pagination = $pagination;

        $this->template->content = $content;
        $this->template->menu = $this->menu;

        // Получаем номер страницы 
        $listNum = $this->request->param('id') ? $this->request->param('id') : 1;
        $this->template->content->listNum = $listNum;
    }

    public function action_list()
    {
        $this->action_index();
    }

    public function action_addImage()
    {
        $this->auto_render = FALSE;
        $errors = '';

        $path = './public/images/'.$this->alias;
        if (!file_exists($path)) {
            mkdir($path, 0700, true);
        }

        if (count($_FILES)) {
            foreach ($_FILES as $file) {

                if (!$file['size']) {
                    exit(json_encode('Файл поврежден'));
                }
                if ($file['size'] > 2 * pow(10, 6)) {
                    exit(json_encode('Файл превышает допустимый размер (2 Мб)'));
                }

                $image = Image::factory($file['tmp_name']);
                $ext = substr($file['name'], strrpos($file['name'], '.') + 1);

                if (!in_array($image->type, array(1, 2, 3))) {
                    exit(json_encode('Некорректный тип файла'));
                }

                if ($image->width > 1200 || $image->height > 763) {
                    $image->resize(1200, 763, Image::AUTO);
                }

                $newName = date('YmdHis').rand(100, 1000);

                $fullPath = $path.'/'.$newName.'.'.$ext;

                if ($image->save($fullPath)) {
                    Model::factory('image')->add(array(
                        'name' => $newName.'.'.$ext,
                        'essence' => $this->alias,
                        'id_es' => $this->request->post('id_es')
                    ));

                    exit(json_encode('Файл добавлен '.$path.'/'.$newName.'.'.$ext));
                }
            }
        } else {
            $errors = 'Файл не выбран';
        }

        $this->response->body(json_encode($errors));
    }

    public function action_delImage()
    {
        $this->auto_render = FALSE;

        if ($id = $this->request->post('id')) {

            $model = Model::factory('image');
            $image = $model->get($id);
            if ($model->delete($id)) {
                unlink('./public/images/'.$image['essence'].'/'.$image['name']);
                $this->response->body(1);
            } else {
                $this->response->body(0);
            }
        }
    }

    public function action_addParent()
    {

        $this->auto_render = FALSE;

        // Загружаем модель отношений категории
        $model = Model::factory('Relation');

        $parRec = Arr::get($_GET, 'par_rec', 0);
        $dotRec = Arr::get($_GET, 'dot_rec', 0);
        $parEss = Arr::get($_GET, 'par_ess', 0);
        $dotEss = Arr::get($_GET, 'dot_ess', 0);

        $fields = array(
            'par_record' => $parRec,
            'dot_record' => $dotRec,
            'par_essence' => $parEss,
            'dot_essence' => $dotEss
        );

        return ($model->save($fields));
    }

    public function action_delParent()
    {
        $this->auto_render = FALSE;

        $model = Model::factory('Relation');

        $parRec = Arr::get($_GET, 'par_rec', 0);
        $dotRec = Arr::get($_GET, 'dot_rec', 0);
        $parEss = Arr::get($_GET, 'par_ess', 0);
        $dotEss = Arr::get($_GET, 'dot_ess', 0);

        $fields = array(
            'par_record' => $parRec,
            'dot_record' => $dotRec,
            'par_essence' => $parEss,
            'dot_essence' => $dotEss
        );

        return $model->delWhere($fields);
    }
}