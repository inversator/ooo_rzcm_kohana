<?php
defined('SYSPATH') OR die('Прямой доступ запрещен');

class Controller_Admin_Login extends Controller_Template
{
    public $template = 'admin/login';

    public function action_index()
    {
        // Проверяем авторизировался пользователь или нет
        if (Auth::instance()->logged_in('admin')) {
            $this->redirect('/admin/');
        }
    }

    public function action_check()
    {
        if ($post = $this->request->post()) {

            $this->auto_render = FALSE;

            // Если значения логина и пароля не пустые то авторизируемся на сайте
            if (!empty($post['username']) && !empty($post['password'])) {
                Auth::instance()->login($post['username'], $post['password']);
            }
         
            
            if (Auth::instance()->logged_in('admin')) {
                $this->response->body(json_encode(array(1, '/admin/')));
            } else {
                $this->response->body(json_encode(array(0, 'Неверный логин или пароль')));
            }
        }
    }

    public function action_logout()
    {
        // Разлогиниваем пользователя
        Auth::instance()->logout();
        // Редиректим его на страницу авторизации
        $this->redirect('/admin/login');
    }
}