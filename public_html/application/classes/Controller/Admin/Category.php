<?php
defined('SYSPATH') OR die('Прямой доступ запрещен');

class Controller_Admin_Category extends Controller_Admin_Base
{
    public $menu = '';
    public $pTitle_ru = 'Категории';
    public $bTitle_ru = 'Категории';
    public $alias = 'category';
    protected $essence = 'Категория';
    protected $genus = 'female';
    protected $model_name = 'category';

    public function before()
    {
        parent::before();

        $type = 3;
        $menuUrl = '/admin/menu/get/' . $type . '?controller=' . $this->alias;
        $menu = Request::factory($menuUrl);
        $this->menu = $menu->execute();
        if (I18n::lang() == 'ru') {
            $this->template->bTitle = $this->bTitle;
        } else {
            $this->template->bTitle = 'Category';
        }

    }

    public function action_edit()
    {
        parent::action_edit();

        // Получаем изображения
        $modelImage = Model::factory('image');
        $id = $this->request->param('id');

        $images = $modelImage->getImage($this->alias, $id);
        $this->template->content->images = $images;

        // Получаем список категорий
        $catList = Model::factory('category')->getAll();
        $this->template->content->catList = $catList;

        // Получаем список родительских категорий
        $relModel = Model::factory('Relation');
        $relResult = $relModel->getWhere(array('dot_record' => $id, 'dot_essence' => 'category'));
        $parList = array();

        foreach ($relResult as $relation) {
            if (isset($relation['par_cat'])) $parList[] = $relation['par_cat'];
        }

        $this->template->content->parList = $parList;
    }

    public function action_delete()
    {
        $id = Arr::get($_POST, 'id', 0);

        // Удаляем связи с изображениями и изображения
        $images = Model::factory('image')->getImage($this->alias, $id);
        foreach ($images as $image) {
            unlink('./public/images/' . $image['essence'] . '/' . $image['name']);
        }
        Model::factory('image')->delWhere(array('id_es' => $id, 'essence' => $this->alias));

        //Удаляем связи
        Model::factory('Relation')->unlink($id, $this->alias);

        parent::action_delete();
    }
}