<?php
defined('SYSPATH') or die('Прямой ступ запрещен');

class Controller_Admin_Unit extends Controller_Admin_Base
{
    public $menu = '';
    public $pTitle = 'Материалы';
    public $bTitle = 'Материалы';
    public $alias = 'unit';
    protected $essence = 'Материал';
    protected $genus = 'male';
    protected $model_name = 'Unit';

    public function before()
    {
        parent::before();

        $type = 3;
        $menuUrl = '/admin/menu/get/'.$type.'?controller='.$this->alias;
        $menu = Request::factory($menuUrl);
        $this->menu = $menu->execute();

        $this->template->bTitle = $this->bTitle;
    }

    public function action_edit()
    {
        parent::action_edit();

        // Получаем изображения
        $modelImage = Model::factory('image');
        $id = $this->request->param('id');

        $images = $modelImage->getImage($this->alias, $id);
        $this->template->content->images = $images;

        // Получаем список записей
        $list = Model::factory($this->model_name)->getAll();
        $this->template->content->list = $list;

        // Получаем список категорий
        $list = Model::factory('Category')->getAll();
        $this->template->content->catList = $list;

        // Получаем список родительских страниц
        $relModel = Model::factory('Relation');
        $relResult = $relModel->getWhere(array('dot_record' => $id, 'dot_essence' => 'unit'));

        // Получаем список свойств единицы
        $unitProperties = Model::factory($this->model_name)->getProperties($id);
        $this->template->content->unitProp = $unitProperties->as_array();

        // Получаем список всех свойств
        $properties = Model::factory('Property')->getKeyValue('id', 'alias');
        $this->template->content->properties = $properties;

        $parList = array();
        $parCatList = array();

        foreach ($relResult as $relation) {

            if ($relation['par_essence'] == $this->alias) {
                $parList[] = $relation['par_record'];
            } else {
                $parCatList[] = $relation['par_record'];
            }
        }

        $this->template->content->parCatList = $parCatList;
        $this->template->content->parList = $parList;
    }

    public function action_addProperty()
    {
        $this->auto_render = FALSE;

        $fields = array(
            'id_unit' => Arr::get($_GET, 'id_unit', 0),
            'id_prop' => Arr::get($_GET, 'id_prop', 0),
            'value' => Arr::get($_GET, 'value', 0),
        );

        $result = Model::factory('propvalue')->add($fields);

        $this->response->body(json_encode($result));
    }

    public function action_getProperties()
    {
        $this->auto_render = FALSE;

        $result = Model::factory($this->alias)
            ->getProperties(Arr::get($_GET, 'id', 0));

        $this->response->body(json_encode(array_reverse($result->as_array())));
    }

    public function action_deleteProp()
    {
        $this->auto_render = FALSE;

        $message = '';
        $id = $this->request->post('id');

        if (Model::factory('propvalue')->delete($id)) {
            $message = 'Значение удалено';
        } else {
            $message = 'Не удалось удалить значение';
        }

        $this->response->body($message);
    }

    public function action_changePos()
    {
        $this->auto_render = FALSE;

        $fields = array(
            'pos' => Arr::get($_GET, 'pos', 0),
            'id' => Arr::get($_GET, 'id', 0)
        );

        return Model::factory('propvalue')->save($fields);
    }

    public function action_delete()
    {
        $id = Arr::get($_POST, 'id', 0);
        
        // Удаляем связи со свойствами
        Model::factory('propvalue')->delWhere(array('id_unit' => $id));
        
        // Удаляем связи с изображениями и изображения
        $images = Model::factory('image')->getImage($this->alias, $id);
        foreach ($images as $image) {
            @unlink('./public/images/'.$image['essence'].'/'.$image['name']);
        }
        Model::factory('image')->delWhere(array('id_es' => $id, 'essence' => $this->alias));

        //Удаляем связи
        Model::factory('Relation')->unlink($id, $this->alias);

        parent::action_delete();
    }
}