<?php

class Controller_Sender extends Controller_Base
{
    private $rules = array(
        'question' => array(
            'name' => array(
                array('not_empty'),
                array('max_length', array(':value', 100)),
                array('english_rus')
            ),
            'phone' => array(
                array('phone')
            ),
            'email' => array(
                array('email')
            ),
            'comment' => array(
                array('max_length', array(':value', 1000))
            )
        ),
        'callMe' => array(
            'phone' => array(
                array('not_empty'),
                array('phone')
            ),
        )
    );

    public function action_check()
    {
        $this->auto_render = FALSE;

        $post = new Validation($this->request->post());

        $post
            ->rule('name', 'max_length', array(':value', 100))
            ->rule('name', 'english_rus')
            ->rule('phone', 'phone')
            ->rule('phone', 'numeric')
            ->rule('email', 'email')
            ->rule('comment', 'max_length', array(':value', 1000))
        ;

        if ($post->check()) {
            $this->response->body(json_encode(0));
        } else {
            $this->response->body(json_encode($post->errors('comments')));
        }
    }

    public function action_send()
    {
        $this->auto_render = FALSE;

        $post = $this->validation($this->request->post());
        $response = array();

        if ($post->check()) {

            $config = KOHANA::$config->load('email');
            Email::connect($config);

            $view = View::factory('mail/'.$this->request->post('subject'));
            $view->post = $this->request->post();

            $to = Kohana::$config->load('main.email');

            $subject = $this->request->post('type');
            $from = array('info@hostel24-ryazan.ru', 'HOSTEL24');

            $sendMessage = $view;


            $response[1] = Email::send($to, $from, $subject, $sendMessage,
                    $html = true);

            // Если верно отправляем копию
            if ($response[1]) {
                Email::send(Kohana::$config->load('main.email2'), $from,
                    $subject, $sendMessage, $html = true);
            }

            $response[0] = 1;
        } else {
            $response[0] = 0;
            $response[1] = $post->errors('comments');
        }

        $this->response->body(json_encode($response));
    }

    public function validation($array)
    {
        $validation = Validation::factory($array);
        $_rules = $this->rules[$array['subject']];

        foreach ($_rules as $field => $rules) {
            foreach ($rules as $rule) {

                $validation->rule($field, $rule[0], Arr::get($rule, 1));
            }
        }

        return $validation;
    }
}