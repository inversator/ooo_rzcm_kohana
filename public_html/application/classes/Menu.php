<?php
defined('SYSPATH') or die('Прямой доступ запрещен');

class Menu
{
    public $directory = '/';

    public static function liMark($alias, $label)
    {
        $directory = strtolower(Request::current()->directory());

        $query = Request::current()->query();

        if ($directory) {
            $directory = "/".$directory;
            $slug = strtolower($query['controller']);
        } else {
            $slug = strtolower($query['controller']);
        }

        $active = ($slug == $alias) ? " class='active'" : "";

        return "<li".$active."><a href='".$directory."/".$alias."'>".$label."</a></li>";
    }

    public static function liDropMark($alias, $label, $subs)
    {
        $directory = strtolower(Request::current()->directory());

        $query = Request::current()->query();

        if ($directory) {
            $directory = "/".$directory;
            $slug = strtolower($query['controller']);
        } else {
            $slug = strtolower($query['controller']);
        }

        $active = ($slug == $alias) ? " active" : "";

        $result = "<li class='dropdown".$active."'>"
            ."<a href='".$directory."/".$alias."'>".$label."" //".$directory."/".$alias."
            ."</a><a href='#' class='dropdown-toggle' data-toggle='dropdown'><b class='caret'></b></a>";

        $result .= '<ul class="dropdown-menu">';

        foreach ($subs as $sub) {
            $result .= '<li><a href="/'.$alias.'/'.$sub['url'].'">'.$sub['title'].'</a></li>';
        }
        $result .= '</ul></li>';

        return $result;
    }

    public static function aList($list)
    {

        $html = '';

        $slug = Request::initial()->param('slug');
        $slug2 = Request::initial()->param('slug2');

        foreach ($list as $item) {

            if ($item['url'] == $slug) {
                $active = ' active';
            } else {
                $active = '';
            }

            $html .= "<a href=".'/'.$item['url']." class='list-group-item".$active."'>".$item['pTitle_' . I18n::lang()];
            if (count($item['dot_records'])) {
                $html .= '<span onclick="return false;" class="glyphicon glyphicon-chevron-down expand" data-toggle="collapse" data-target="#list-'.$item['id'].'"></span>';
            }
            $html .= '</a>';

            if (count($item['dot_records'])) {

                $dotList = '';
                $in = '';

                foreach ($item['dot_records'] as $dot_record) {
                    if ($dot_record['url'] == $slug2 && $item['url'] == $slug) {
                        $active = ' active';
                        $in = ' in';
                    } else {
                        $active = '';
                    }
                    $dotList .= "<a href = ".'/'.$item['url'].'/'.$dot_record['url']." class = 'list-group-item child".$active."'>".$dot_record['pTitle_' . I18n::lang()]."</a>";
                }

                $html .= '<div id="list-'.$item['id'].'" class="collapse'.$in.'">'.$dotList.'</div>';
            }
        }
        echo $html;
    }
    public static function bList($list)
    {

        $html = '';

        $slug = Request::initial()->param('catProduct');
        $slug2 = Request::initial()->param('product');

        foreach ($list as $item) {
            if ($item['url'] == $slug) {
                $active = ' active';
            } else {
                $active = '';
            }

            $html .= "<a href=".'/catalog/'.$item['url']." class='list-group-item".$active."'>".$item['pTitle_' . I18n::lang()];
            if (count($item['dot_records'])) {
                $html .= '<span onclick="return false;" class="glyphicon glyphicon-chevron-down expand" data-toggle="collapse" data-target="#list-'.$item['id'].'"></span>';
            }
            $html .= '</a>';

            if (count($item['dot_records'])) {

                $dotList = '';
                $in = '';

                foreach ($item['dot_records'] as $dot_record) {
                    if ($dot_record['url'] == $slug2 && $item['url'] == $slug) {
                        $active = ' active';
                        $in = ' in';
                    } else {
                        $active = '';
                    }
                    $dotList .= "<a href = ".'/catalog/'.$item['url'].'/'.$dot_record['url']." class = 'list-group-item child".$active."'>".$dot_record['pTitle_' . I18n::lang()]."</a>";
                }

                $html .= '<div id="list-'.$item['id'].'" class="collapse'.$in.'">'.$dotList.'</div>';
            }
        }
        echo $html;
    }
}