    <?php
    defined('SYSPATH') OR die('No direct access allowed.');
     
    return array(
        'driver'       => 'ORM',
        'hash_method'  => 'sha256',
        'hash_key'     => 'msjg7394nsdhkgk58wnalpfis',
        'lifetime'     => 129600, // запоминаем на две недели 60*60*24*14
        'session_type' => Session::$default,
        'session_key'  => 'auth_user',
    );