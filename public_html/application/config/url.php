<?php defined('SYSPATH') OR die('No direct script access.');

return array(

    'trusted_hosts' => array(
        'ru.rzcm-kohana',
        'en.rzcm-kohana',
        'rzcm.ru',
        'en.rzcm.ru',
        'bb.rzcm-kohana',
    ),

);
