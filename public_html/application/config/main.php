<?php
defined('SYSPATH') or die('Прямой доступ к файлу запрещен');

return array(
    'siteName' => 'Ltd «Ryazcvetmet»',
    'email' => 'rzcm@rzcm.ru',
    'email2' => 'aleksey.markov.msk@gmail.com',
    'site-ru' => 'http://ru.rzcm-kohana/',
    'site-en' => 'http://en.rzcm-kohana/',
);