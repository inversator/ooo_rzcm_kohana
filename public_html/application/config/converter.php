<?php
defined('SYSPATH') or die('Прямой доступ к файлу запрещен');

return array(
    'genus' => array(
        'average' => 'о',
        'male' => '',
        'female' => 'a',
        'many' => 'ы',
    ),
    'delivery' => array (
        'self' => 'Самовывоз',
        'courier' => 'Курьер',
        'post' => 'Почтовый перевод'
    ),
    
);

