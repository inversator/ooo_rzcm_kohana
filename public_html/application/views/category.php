
<div class="row row-offcanvas row-offcanvas-right">

    <div class="col-xs-12 col-sm-9">
        <p class="pull-right visible-xs">
            <button class="btn btn-primary btn-xs" data-toggle="offcanvas" type="button">Список категорий</button>
        </p>

        <div class="page-header">
            <h1><?php echo $title; ?></h1>
            <p class="lead"><?php echo $desc; ?></p>
        </div>

        <?php if (count($units)): ?>
            <div class="row">

                <div class="paginationBlock"><?php echo $pagination; ?></div>
                <hr>
                <div class="filter" slug="<?php echo $slug; ?>"  cat="<?php echo $id; ?>"></div>
                <div id="unitsBlock">
                    <?php foreach ($units as $unit): ?>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail unit">
                                <div class="unitListImg">
                                    <a href='<?php echo "/".$url."/".$unit['url']; ?>'><img alt="" class="unitImg" src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a>
                                </div>
                                <div class="caption unitListBlock">
                                    <h4 class="unitListName"><a href='<?php echo "/".$url."/".$unit['url']; ?>'><?php echo $unit['pTitle']; ?></a></h4>
                                    <div class="unitListDesc"><?php echo Room::subDesc($unit['desc']); ?></div>
                                    <div class="unitButtons">
                                        <a role="button" class="btn btn-primary" onclick="addToCart(<?php echo $unit['id']; ?>);
                                                return false;">В корзину</a>
                                           <?php if ($unit['price']): ?>
                                            <div class="priceCatList"><?php echo $unit['price']; ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php else: ?>
            <p class="alert alert-warning">В этой категории нет товаров</p>
        <?php endif; ?>

        <div class="paginationBlock"><?php echo $pagination; ?></div>
        <div class="fullText"><?php echo $text; ?></div>
    </div>

    <div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
        <?php echo $rigthMenu; ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        // Снимаем все чекбоксы
        $('.filter input:checkbox').attr('checked', false);
    })
</script>
