<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">

        <link rel="shortcut icon" href="favicon.ico">

        <script src="<?php echo URL::base(); ?>public/admin/js/jquery.js"></script>
        <script src="<?php echo URL::base(); ?>public/admin/js/jquery.datetimepicker.full.js" type="text/javascript"></script>

        <!-- Подключаем бутстрап -->
        <link href="<?php echo URL::base(); ?>bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo URL::base(); ?>bootstrap/js/bootstrap.js"></script>

        <script type="text/javascript" src="<?php echo URL::base(); ?>public/admin/js/tinymce/tinymce.min.js"></script>
        <script>tinymce.init({
                mode: "textareas",
                selector: 'textarea',
                language: 'ru',
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    "insertdatetime media table contextmenu paste code wordcount image"
                ],
                extended_valid_elements: "anchor[name|id|class],script[language|type|src],button[*],div[*],error[*],input[*],noindex[*],a[*],span[*]",
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code'
            });
        </script>

        <link href="<?php echo URL::base(); ?>public/admin/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo URL::base(); ?>public/admin/css/main.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo URL::base(); ?>public/admin/js/main.js" type="text/javascript"></script>

        <title><?php echo $bTitle; ?></title>
    </head>

    <body>

        <?php echo $navigation; ?>

        <div class="container-fluid">
            <div class="row">

                <?php echo $menu; ?>
                <?php echo $content; ?>       

            </div>
        </div>
    </body>
</html>