<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>
    <h2 class="sub-header"><?php echo isset($page['pTitle_ru']) ? $page['pTitle_ru'] : 'Новый материал'; ?></h2>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#fieldsTabRu" data-toggle="tab">Ru</a></li>
        <li><a href="#fieldsTabEn" data-toggle="tab">En</a></li>
        <li><a href="#textTabRu" data-toggle="tab">Текст</a></li>
        <li><a href="#textTabEn" data-toggle="tab">Текст En</a></li>

        <li><a href="#propertiesTab" data-toggle="tab">Свойства</a></li>
        <li><a href="#relationTab" data-toggle="tab">Связанные материалы</a></li>
        <li><a href="#relationTabCat" data-toggle="tab">Родительские категории</a></li>
        <li><a href="#imagesTab" data-toggle="tab">Изображения</a></li>
    </ul>

    <div class="ajax-respond"></div>
    <form role="form" id="pageForm" enctype="multipart/form-data">
        <div id="result_id"></div>

        <div class="tab-content">
            <div class="tab-pane active" id="fieldsTabRu">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" class="form-control" name="pTitle_ru" placeholder="Название" value="<?php
                            echo (isset($page['pTitle_ru'])) ? $page['pTitle_ru'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Заголовок в браузере</label>
                            <input type="text" class="form-control" name="bTitle_ru" placeholder="Заголовок в браузере"
                                   value="<?php
                                   echo (isset($page['bTitle_ru'])) ? $page['bTitle_ru'] : '';
                                   ?>">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="off" type="checkbox" <?php if (empty($page['off'])) echo "checked"; ?>>
                                Опубликован
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="recommended"
                                       type="checkbox" <?php if (!empty($page['recommended'])) echo "checked"; ?>>
                                Рекомендуемый
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                На каком сайте выводить?
                                <select name="lang">

                                    <option <?php if(isset($page['lang']) && $page['lang']  == 'ru') echo "selected"; ?> value="ru">RU</option>
                                    <option <?php if(isset($page['lang']) && $page['lang']  == 'en') echo "selected"; ?> value="en">EN</option>
                                </select>
                            </label>
                        </div>

                        <div class="form-group">
                            <label>Ключевые слова</label>
                            <input type="text" class="form-control" name="meta_k" placeholder="Ключевые слова"
                                   value="<?php
                                   echo (isset($page['meta_k'])) ? $page['meta_k'] : '';
                                   ?>">
                        </div>

                        <div class="form-group">
                            <label>Описание для поисковых систем</label>
                            <input type="text" class="form-control" name="meta_d"
                                   placeholder="Описание для поисковых систем" value="<?php
                            echo (isset($page['meta_d'])) ? $page['meta_d'] : '';
                            ?>">
                        </div>
                        <div class="form-group">

                            <label>Дата создания</label>
                            <input id="datetimepicker" type="text" class="form-control" name="datetime" value="<?php
                            echo isset($page['date']) ? date('d.m.Y H:i',
                                $page['date']) : date('d.m.Y H:i');
                            ?>"/>
                            <p class="help-block">*Служит для сортировки старые/новые</p>
                        </div>
                        <div class="form-group">

                            <label>URL</label>
                            <input type="text" class="form-control" name="url" placeholder="url" value="<?php
                            echo (isset($page['url'])) ? $page['url'] : '';
                            ?>">
                            <p class="help-block">*Название страницы в адресной строке</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormUnit('result_id', 'pageForm', '/admin/unit/save');
                        return true;">Сохранить
                </button>
            </div>
            <div class="tab-pane" id="fieldsTabEn">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" class="form-control" name="pTitle_en" placeholder="Название" value="<?php
                            echo (isset($page['pTitle_en'])) ? $page['pTitle_en'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Заголовок в браузере</label>
                            <input type="text" class="form-control" name="bTitle_en" placeholder="Заголовок в браузере"
                                   value="<?php
                                   echo (isset($page['bTitle_en'])) ? $page['bTitle_en'] : '';
                                   ?>">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="off" type="checkbox" <?php if (empty($page['off'])) echo "checked"; ?>>
                                Опубликован
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="recommended"
                                       type="checkbox" <?php if (!empty($page['recommended'])) echo "checked"; ?>>
                                Рекомендуемый
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Ключевые слова</label>
                            <input type="text" class="form-control" name="meta_k" placeholder="Ключевые слова"
                                   value="<?php
                                   echo (isset($page['meta_k'])) ? $page['meta_k'] : '';
                                   ?>">
                        </div>

                        <div class="form-group">
                            <label>Описание для поисковых систем</label>
                            <input type="text" class="form-control" name="meta_d"
                                   placeholder="Описание для поисковых систем" value="<?php
                            echo (isset($page['meta_d'])) ? $page['meta_d'] : '';
                            ?>">
                        </div>
                        <div class="form-group">

                            <label>Дата создания</label>
                            <input id="datetimepicker" type="text" class="form-control" name="datetime" value="<?php
                            echo isset($page['date']) ? date('d.m.Y H:i',
                                $page['date']) : date('d.m.Y H:i');
                            ?>"/>
                            <p class="help-block">*Служит для сортировки старые/новые</p>
                        </div>
                        <div class="form-group">

                            <label>URL</label>
                            <input type="text" class="form-control" name="url" placeholder="url" value="<?php
                            echo (isset($page['url'])) ? $page['url'] : '';
                            ?>">
                            <p class="help-block">*Название страницы в адресной строке</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormUnit('result_id', 'pageForm', '/admin/unit/save');
                        return true;">Сохранить
                </button>
            </div>
            <div class="tab-pane" id="textTabRu">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Краткое описание</label>
                            <textarea id="tinyDescRu" class="form-control" name="desc_ru"><?php
                                echo (isset($page['desc_ru'])) ? $page['desc_ru'] : '';
                                ?></textarea>
                            <p class="help-block">*Используется при выводе страниц в списке и как верхний блок на
                                странице с материалом</p>
                        </div>

                        <div class="form-group">
                            <label>Текст</label>
                            <textarea id="tinyTextRu" class="fullText" class="form-control" name="text_ru"><?php
                                echo (isset($page['text_ru'])) ? $page['text_ru'] : '';
                                ?></textarea>
                            <p class="help-block">*Выводится на странице с материалом</p>
                        </div>

                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormUnit('result_id', 'pageForm', '/admin/unit/save');
                        return true;">Сохранить
                </button>
            </div>
            <div class="tab-pane" id="textTabEn">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Краткое описание</label>
                            <textarea id="tinyDescEn" class="form-control" name="desc_en"><?php
                                echo (isset($page['desc_en'])) ? $page['desc_en'] : '';
                                ?></textarea>
                            <p class="help-block">*Используется при выводе страниц в списке и как верхний блок на
                                странице с материалом</p>
                        </div>

                        <div class="form-group">
                            <label>Текст</label>
                            <textarea id="tinyTextEn" class="fullText" class="form-control" name="text_en"><?php
                                echo (isset($page['text_en'])) ? $page['text_en'] : '';
                                ?></textarea>
                            <p class="help-block">*Выводится на странице с материалом</p>
                        </div>

                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormUnit('result_id', 'pageForm', '/admin/unit/save');
                        return true;">Сохранить
                </button>
            </div>

            <div class="tab-pane" id="propertiesTab">
                <?php if (isset($unitProp)): ?>
                    <table class="table table-striped">
                        <thead>
                        <th>Порядок</th>
                        <th>Название свойства</th>
                        <th>Значение</th>
                        </thead>
                        <tbody id="tBody">
                        <?php
                        if (count($unitProp)):
                            foreach ($unitProp as $prop):
                                ?>

                                <tr id="ItemDel-<?php echo $prop['id']; ?>">
                                    <td><input class="form-control propPos"
                                               onchange="changePos(<?php echo $prop['id']; ?>)"
                                               id="pos<?php echo $prop['id']; ?>" value="<?php echo $prop['pos']; ?>"/>
                                    </td>
                                    <td><?php echo $prop['alias']; ?></td>
                                    <td><?php echo $prop['value']; ?></td>
                                    <td>
                                        <button type="button"
                                                onclick="DelObjectAjax(<?php echo $prop['id']; ?>, '/admin/unit/deleteProp/', 'ItemDel-<?php echo $prop['id']; ?>')
                                                        return true;" class="btn btn-danger">Удалить
                                        </button>
                                    </td>
                                </tr>

                            <?php
                            endforeach;
                        else:
                            ?>
                            <div class="alert alert-warning fade in">Свойства не заданы</div>
                        <?php endif; ?>
                        </tbody>
                    </table>

                    <form id="formProp" class="row">
                        <div class="col-xs-5">
                            <?php
                            echo Form::select('properties', $properties, 1,
                                array('class' => 'form-control', 'onchange' => 'LoadInput()',
                                    'id' => 'propSelect'));
                            ?>
                        </div>
                        <div class="col-xs-5" id="inputId"></div>
                        <div class="col-xs-2">
                            <button type="button" onclick="addPropery()" class="btn btn-success">Добавить+</button>
                        </div>
                    </form>
                <?php else: ?>
                    <p class="help-block">*Для добавления свойств сохраните материал и перейдите к редактированию</p>
                <?php endif; ?>
            </div>

            <div class="tab-pane" id="relationTab">
                <?php if (isset($list)): ?>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Имя материала</th>
                            <th>URL</th>
                            <th>Связать</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($list as $item): if ($item['id'] == $page['id']) {
                            continue;
                        }
                            ?>
                            <tr>
                                <td><?php echo $item['pTitle_ru']; ?></td>
                                <td><?php echo $item['url']; ?></td>
                                <td><input <?php
                                    if (in_array($item['id'], $parList)) {
                                        echo "checked";
                                    }
                                    ?> class="parentCheck<?php echo $item['id']; ?>-unit" type="checkbox"
                                       onchange="parentRec(<?php echo $item['id'] . ", " . $page['id']; ?>, 'unit', 'unit');"/>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте
                        внимательны!), нажимать сохранить не обязательно.</p>
                <?php else: ?>
                    <p class="help-block">*Для создания связей сохраните страницу</p>
                <?php endif; ?>
            </div>

            <div class="tab-pane" id="relationTabCat">
                <?php if (isset($list)): ?>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Имя категории</th>
                            <th>URL</th>
                            <th>Назначить родительской</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($catList as $item): ?>
                            <tr>
                                <td><?php echo $item['pTitle_ru']; ?></td>
                                <td><?php echo $item['url']; ?></td>
                                <td><input <?php
                                    if (in_array($item['id'], $parCatList)) {
                                        echo "checked";
                                    }
                                    ?> class="parentCheck<?php echo $item['id']; ?>-category" type="checkbox"
                                       onchange="parentRec(<?php echo $item['id'] . ", " . $page['id']; ?>, 'category', 'unit');"/>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте
                        внимательны!), нажимать сохранить не обязательно.</p>
                <?php else: ?>
                    <p class="help-block">*Для создания связей сохраните страницу</p>
                <?php endif; ?>
            </div>

            <div class="tab-pane" id="imagesTab">
                <h3>Изображения</h3>

                <?php if (isset($page['id'])): ?>
                    <div class="form-group">
                        <div class="imgConteiner">
                            <?php
                            if (!isset($images) || !count($images)):
                                ?>
                                <img src="/public/images/unit/no-img.png" alt="Нет изображения" class="img-thumbnail"/>
                            <?php else: ?>
                                <?php foreach ($images as $image): ?>
                                    <div id="imgBlock-<?php echo $image['id']; ?>" class="imgBlock img-thumbnail">

                                        <img class="imgTeg" src="/public/images/unit/<?php echo $image['name']; ?>"/>
                                        <div class="imgControl">
                                            <div class="imgDel">
                                                <div url="/admin/unit/delImage" idimg="<?php echo $image['id']; ?>"><a>Удалить</a>
                                                </div>
                                            </div>
                                            <div class="imageCheck">
                                                <input id_img="<?php echo $image['id']; ?>"
                                                       id_es="<?php echo $image['id_es']; ?>" <?php if ($image['main']) echo 'checked'; ?>
                                                       type="radio" name="mainImg" data-toggle="tooltip"
                                                       data-placement="top" title="Выбрать главным"/>
                                            </div>
                                        </div>

                                    </div>
                                <?php
                                endforeach;
                            endif;
                            ?>
                        </div>

                        <hr>

                        <button type="button" id="addImg" class="btn btn-primary" data-toggle="modal"
                                data-target="#myModal">
                            Добавить
                        </button>

                        <!-- Модаль -->

                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div id="modal_result_id"></div>
                                        <h4 class="modal-title" id="myModalLabel">Добавить изображение</h4>
                                    </div>
                                    <div class="modal-body">
                                        <label for="exampleInputFile">Изображение категории</label>
                                        <input name="img" type="file" id="inputFile">
                                        <p class="help-block">*Допустимые форматы PNG, JPG</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                        </button>
                                        <button id="uploadFile" essence="unit" type="button" class="btn btn-primary">
                                            Загрузить файл
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте
                            внимательны!), нажимать сохранить не обязательно.</p>
                    </div>
                <?php else: ?>
                    <p class="alert alert-warning fade in">* Для добавления изображений сохраните материал и перейдите к
                        её редактированию</p>
                <?php endif; ?>
            </div>

        </div>

        <input type="hidden" name="id" value="<?php
        echo (isset($page['id'])) ? $page['id'] : '';
        ?>"/>
    </form>


</div>
<script type="text/javascript">
    var act = '<?php echo $act; ?>';
    LoadInput();
    jQuery('#datetimepicker').datetimepicker({
        format: 'd.m.Y H:i'
    });</script>