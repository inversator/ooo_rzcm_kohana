<?php if (isset($_GET['submit'])) { ?>

    <p class="bg-success">На указанный Вами email было отправлено письмо. Пройдите по ссылке указанной в нем чтобы завершить регистрацию</p>

<?php } ?>

<div class="row">
    <div class="col-xs-6">

        <form id="loginForm" role="form" class="form-signin" method="POST">
            <h2 class="form-signin-heading">Авторизация</h2>
            <div id="result_id"></div>

            <input name='username' type="text" autofocus="" required="" placeholder="Логин или email" class="form-control">
            <input name='password' type="password" required="" placeholder="Пароль" class="form-control">
<!--            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Запомнить меню
            </label>-->
            <button type="submit" class="btn btn-lg btn-primary btn-block" onclick="AjaxRegForm('result_id', 'loginForm', '/users/login');
                return false;">Войти</button>
        </form>
    </div>
    <div class="col-xs-6 note">
        <h3 id="grid-intro">Правила авторизации</h3>
        <p>Для входа на сайт необходимо заполнить форму <a href="/users/registration">регистрации</a> и пройти по ссылке отправленной на Ваш адрес электронной почты, после чего Ваш аккаунт будет подтвержден.</p>
        <p>Если Вы выполнили указанные действия но авторизация не происходит обратитесь к администратору.</p>
        <button type="submit" class="btn btn-lg btn-primary btn-block" id="contactForm">Написать администратору</button>
    </div>
</div>
