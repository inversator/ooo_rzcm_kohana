<script type="text/javascript">
    var act = '<?php echo $act; ?>';

</script>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>
    <h2 class="sub-header"><?php echo isset($page['alias']) ? $page['alias'] : 'Новое свойство'; ?></h2>

    <?php if (isset($page['fixed']) && $page['fixed']): ?>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#fieldsTab" data-toggle="tab">Поля</a></li>
            <li><a href="#valuesTab" data-toggle="tab">Значения поля</a></li>
        </ul>
    <?php endif; ?>

    <div class="ajax-respond"></div>

    <div id="result_id"></div>

    <div class="tab-content">
        <div class="tab-pane active" id="fieldsTab">
            <div class="row">
                <div class="col-sm-12">
                    <form role="form" id="pageForm" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" class="form-control" name="alias" placeholder="Название" value="<?php
                            echo (isset($page['alias'])) ? $page['alias'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Псевдоним (название на английском)</label>
                            <input type="text" class="form-control" name="name" placeholder="Псевдоним" value="<?php
                            echo (isset($page['name'])) ? $page['name'] : '';
                            ?>">
                        </div>

                        <div class="checkbox">
                            <label>
                                <input name="fixed" type="checkbox" <?php if (!empty($page['fixed'])) echo "checked"; ?>> С заданными значениями
                                <p class="help-block">*Для внесения значений сохраните свойство и перейдите к его редактированию</p>
                            </label>
                        </div>
                        <input type="hidden" name="id" value="<?php
                        echo (isset($page['id'])) ? $page['id'] : '';
                        ?>" />
                    </form>

                </div>
            </div>
            <button type="button" class="btn btn-success" onclick="AjaxFormRequest('result_id', 'pageForm', '/admin/property/save');
                    return true;">Сохранить</button>
        </div>
        <?php if (isset($items)): ?>
            <div class="tab-pane" id="valuesTab">
                <div class="table-responsive">
                    <?php if (count($items)) { ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Значение</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $num = 1;

                                foreach ($items as $item):
                                    ?>
                                    <tr id='ItemDel-<?php echo $item['id']; ?>'>
                                        <td><?php echo $num++; ?></td>
                                        <td><?php echo $item['value']; ?></td>
                                        <td><button 
                                                class="btn btn-danger" 
                                                onclick="DelObjectAjax('<?php echo $item['id']; ?>', '/admin/property/delFixedValue/', 'ItemDel-<?php echo $item['id']; ?>')
                                                        return true;" type="button">Удалить</button>
                                        </td>
                                    </tr>
                                <?php endforeach;
                                ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                    <table class="table table-striped"></table>
                        <p id="noValue" class="bg-warning p15">-Значений нет-</p>

                    <?php } ?>

                    <div class="row-offcanvas">

                        <form id="addForm">
                            <div class="col-md-2 vLabel">Добавить значение:</div>
                            <div class="col-md-5">
                                <?php
                                echo Form::input('value', '',
                                    array('class' => 'form-control', 'placeholder' => 'Возможное значение',
                                    'required'));
                                echo form::hidden('id_prop', $page['id']);
                                echo Form::hidden('fixed', 1);
                                ?>

                            </div>

                        </form>

                        <div class = "col-md-5">
                            <button class = "btn btn-success" type = "button" 
                                    onclick="AddValue('result_id', 'addForm');
                                            return true;">Добавить +</button>
                        </div>

                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>





</div>