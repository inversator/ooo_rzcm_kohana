<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>
    <h2 class="sub-header">Список</h2>
    <div id="result_id"></div>
    <button class = "btn btn-success" type = "button" onclick="location.href = '/admin/page/new/';
            return true;">Добавить +</button>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Имя страницы</th>
                    <th>URL</th>
                    <th>Редактирование</th>
                    <th>Удаление</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($pages as $page): ?>
                    <tr id='ItemDel-<?php echo $page['id']; ?>'>
                        <td><?php echo $page['id']; ?></td>

                        <td><a href='/admin/page/edit/<?php echo $page['id']; ?>'><?php echo $page['pTitle_ru']; ?></a></td>
                        <td><?php echo $page['url']; ?></td>
                        <td>
                            <button class = "btn btn-primary" type = "button" onclick="location.href = '/admin/page/edit/<?php echo $page['id']; ?>';
                                    return true;">Редактировать</button>
                        </td>

                        <td>                                <button 
                                class="btn btn-danger" 
                                onclick="DelObjectAjax('<?php echo $page['id']; ?>', '/admin/page/delete/', 'ItemDel-<?php echo $page['id']; ?>')
                                        return true;" type="button">Удалить</button></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo isset($pagination) ? $pagination : ''; ?>
    </div>
</div>