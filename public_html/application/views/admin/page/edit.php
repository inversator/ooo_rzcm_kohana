<script type="text/javascript">
    var act = '<?php echo $act; ?>';
</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>
    <h2 class="sub-header"><?php echo isset($page['pTitle_ru']) ? $page['pTitle_ru'] : 'Новая страница'; ?></h2>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#fieldsTab" data-toggle="tab">Рус</a></li>
        <li><a href="#fieldsTabEn" data-toggle="tab">En</a></li>
        <li><a href="#textTab" data-toggle="tab">Текст</a></li>
        <li><a href="#textTabEn" data-toggle="tab">Текст En</a></li>
        <li><a href="#relationTab" data-toggle="tab">Родительские страницы</a></li>
    </ul>

    <div class="ajax-respond"></div>
    <form role="form" id="pageForm" enctype="multipart/form-data">
        <div id="result_id"></div>



        <div class="tab-content">
            <div class="tab-pane active" id="fieldsTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" class="form-control" name="pTitle_ru" placeholder="Название" value="<?php
                            echo (isset($page['pTitle_ru'])) ? $page['pTitle_ru'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Заголовок в браузере</label>
                            <input type="text" class="form-control" name="bTitle_ru" placeholder="Заголовок в браузере" value="<?php
                            echo (isset($page['bTitle_ru'])) ? $page['bTitle_ru'] : '';
                            ?>">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="off" type="checkbox" <?php if (empty($page['off'])) echo "checked"; ?>> Опубликована
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Ключевые слова</label>
                            <input type="text" class="form-control" name="meta_k" placeholder="Ключевые слова" value="<?php
                            echo (isset($page['meta_k'])) ? $page['meta_k'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Описание для поисковых систем</label>
                            <input type="text" class="form-control" name="meta_d" placeholder="Описание для поисковых систем" value="<?php
                            echo (isset($page['meta_d'])) ? $page['meta_d'] : '';
                            ?>">
                        </div>
                        <div class="form-group">
                            <label>Краткое описание</label>
                            <textarea id="tinyDescRu" class="form-control" name="desc_ru"><?php
                                echo (isset($page['desc_ru'])) ? $page['desc_ru'] : '';
                                ?></textarea>
                            <p class="help-block">*Используется при выводе страниц в списке и как верхний блок на странице с материалом</p>
                        </div>

                        <div class="form-group">

                            <label>URL</label>
                            <input type="text" class="form-control" name="url" placeholder="url" value="<?php
                            echo (isset($page['url'])) ? $page['url'] : '';
                            ?>">
                            <p class="help-block">*Название страницы в адресной строке</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormPage('result_id', 'pageForm', '/admin/page/save');
                        return true;">Сохранить</button>
            </div>

            <div class="tab-pane" id="fieldsTabEn">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" class="form-control" name="pTitle_en" placeholder="Название" value="<?php
                            echo (isset($page['pTitle_en'])) ? $page['pTitle_en'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Заголовок в браузере</label>
                            <input type="text" class="form-control" name="bTitle_en" placeholder="Заголовок в браузере" value="<?php
                            echo (isset($page['bTitle_en'])) ? $page['bTitle_en'] : '';
                            ?>">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="off" type="checkbox" <?php if (empty($page['off'])) echo "checked"; ?>> Опубликована
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Ключевые слова</label>
                            <input type="text" class="form-control" name="meta_k" placeholder="Ключевые слова" value="<?php
                            echo (isset($page['meta_k'])) ? $page['meta_k'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Описание для поисковых систем</label>
                            <input type="text" class="form-control" name="meta_d" placeholder="Описание для поисковых систем" value="<?php
                            echo (isset($page['meta_d'])) ? $page['meta_d'] : '';
                            ?>">
                        </div>
                        <div class="form-group">
                            <label>Краткое описание</label>
                            <textarea id="tinyDescEn" class="form-control" name="desc_en"><?php
                                echo (isset($page['desc_en'])) ? $page['desc_en'] : '';
                                ?></textarea>
                            <p class="help-block">*Используется при выводе страниц в списке и как верхний блок на странице с материалом</p>
                        </div>

                        <div class="form-group">

                            <label>URL</label>
                            <input type="text" class="form-control" name="url" placeholder="url" value="<?php
                            echo (isset($page['url'])) ? $page['url'] : '';
                            ?>">
                            <p class="help-block">*Название страницы в адресной строке</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormPage('result_id', 'pageForm', '/admin/page/save');
                        return true;">Сохранить</button>
            </div>

            <div class="tab-pane" id="textTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Текст</label>
                            <textarea id="tinyTextRu" class="fullText" class="form-control" name="text_ru"><?php
                                echo (isset($page['text_ru'])) ? $page['text_ru'] : '';
                                ?></textarea>
                            <p class="help-block">*Выводится на странице с материалом</p>
                        </div>

                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormPage('result_id', 'pageForm', '/admin/page/save');
                        return true;">Сохранить</button>
            </div>

            <div class="tab-pane" id="textTabEn">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Текст</label>
                            <textarea id="tinyTextEn" class="fullText" class="form-control" name="text_en"><?php
                                echo (isset($page['text_en'])) ? $page['text_en'] : '';
                                ?></textarea>
                            <p class="help-block">*Выводится на странице с материалом</p>
                        </div>

                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormPage('result_id', 'pageForm', '/admin/page/save');
                        return true;">Сохранить</button>
            </div>

            <div class="tab-pane" id="relationTab">
                <?php if (isset($list)): ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Имя страницы</th>
                                <th>URL</th>
                                <th>Назначить родительской</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            foreach ($list as $item): if ($item['id'] == $page['id']) {
                                    continue;
                                }
                                ?>
                                <tr>
                                    
                                    <td><?php echo $item['pTitle_ru']; ?></td>
                                    <td><?php echo $item['url']; ?></td>
                                    <td><input <?php
                                    
                                        if (in_array($item['id'], $parList)) {
                                            echo "checked";
                                        }
                                        ?> 
                                            class="parentCheck<?php echo $item['id']; ?>-page"    
                                            id="parentCheck<?php echo $item['id']; ?>" type="checkbox" onchange="parentRec(<?php echo $item['id'].", ".$page['id']; ?>, 'page', 'page');"  /></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте внимательны!), нажимать сохранить не обязательно.</p>
                <?php else: ?>
                    <p class="help-block">*Для создания связей сохраните страницу</p>
                <?php endif; ?>
            </div>

        </div>

        <input type="hidden" name="id" value="<?php
        echo (isset($page['id'])) ? $page['id'] : '';
        ?>" />
    </form>



</div>