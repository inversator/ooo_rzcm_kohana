<script type="text/javascript">
    var act = '<?php echo $act; ?>';

</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>
    <h2 class="sub-header"><?php echo isset($page['pTitle']) ? $page['pTitle'] : 'Новая категория'; ?></h2>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#fieldsTab" data-toggle="tab">Поля</a></li>
        <li><a href="#textTab" data-toggle="tab">Текст</a></li>
        <li><a href="#relationTab" data-toggle="tab">Родительские категории</a></li>
        <li><a href="#imagesTab" data-toggle="tab">Изображения</a></li>
    </ul>

    <div class="ajax-respond"></div>
    <form role="form" id="pageForm" enctype="multipart/form-data">
        <div id="result_id"></div>



        <div class="tab-content">
            <div class="tab-pane active" id="fieldsTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" class="form-control" name="pTitle" placeholder="Название" value="<?php
                            echo (isset($page['pTitle'])) ? $page['pTitle'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Заголовок в браузере</label>
                            <input type="text" class="form-control" name="bTitle" placeholder="Заголовок в браузере" value="<?php
                            echo (isset($page['bTitle'])) ? $page['bTitle'] : '';
                            ?>">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="off" type="checkbox" <?php if (empty($page['off'])) echo "checked"; ?>> Опубликована
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Ключевые слова</label>
                            <input type="text" class="form-control" name="meta_k" placeholder="Ключевые слова" value="<?php
                            echo (isset($page['meta_k'])) ? $page['meta_k'] : '';
                            ?>">
                        </div>

                        <div class="form-group">
                            <label>Описание для поисковых систем</label>
                            <input type="text" class="form-control" name="meta_d" placeholder="Описание для поисковых систем" value="<?php
                            echo (isset($page['meta_d'])) ? $page['meta_d'] : '';
                            ?>">
                        </div>
                        <div class="form-group">
                            <label>Краткое описание</label>
                            <textarea id="tinyDesc" class="form-control" name="desc"><?php
                                echo (isset($page['desc'])) ? $page['desc'] : '';
                                ?></textarea>
                            <p class="help-block">*Используется при выводе страниц в списке и как верхний блок на странице с материалом</p>
                        </div>

                        <div class="form-group">

                            <label>URL</label>
                            <input type="text" class="form-control" name="url" placeholder="url" value="<?php
                            echo (isset($page['url'])) ? $page['url'] : '';
                            ?>">
                            <p class="help-block">*Название страницы в адресной строке</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormPage('result_id', 'pageForm', '/admin/category/save');
                        return true;">Сохранить</button>
            </div>

            <div class="tab-pane" id="textTab">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Текст</label>
                            <textarea id="tinyText" class="fullText" class="form-control" name="text"><?php
                                echo (isset($page['text'])) ? $page['text'] : '';
                                ?></textarea>
                            <p class="help-block">*Выводится на странице с материалом</p>
                        </div>

                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="AjaxFormPage('result_id', 'pageForm', '/admin/category/save');
                        return true;">Сохранить</button>
            </div>

            <div class="tab-pane" id="relationTab">
                <?php if (isset($catList)): ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Имя категории</th>
                                <th>URL</th>
                                <th>Назначить родительской</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($catList as $cat): if ($cat['id'] == $page['id'])
                                        continue;
                                ?>
                                <tr>
                                    <td><?php echo $cat['pTitle']; ?></td>
                                    <td><?php echo $cat['url']; ?></td>
                                    <td><input <?php
                                        if (in_array($cat['id'], $parList)) {
                                            echo "checked";
                                        }
                                        ?> class="parentCheck<?php echo $cat['id']; ?>" type="checkbox" onchange="parentRec(<?php echo $cat['id'].", ".$page['id']; ?>, 'category', 'category');"  /></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте внимательны!), нажимать сохранить не обязательно.</p>
                <?php else: ?>
                    <p class="help-block">*Для создания связей сохраните страницу</p>
                <?php endif; ?>
            </div>

            <div class="tab-pane" id="imagesTab">
                <h3>Изображения</h3>

                <?php if (isset($page['id'])): ?>
                    <div class="form-group">
                        <div class="imgConteiner">
                            <?php
                            if (!isset($images) || !count($images)):
                                ?>
                                <img src="/public/images/category/no-img.png" alt="Нет изображения" class="img-thumbnail" />
                            <?php else: ?>
                                <?php foreach ($images as $image): ?> 
                                    <div id="imgBlock-<?php echo $image['id']; ?>" class="imgBlock img-thumbnail">

                                        <img class="imgTeg" src="/public/images/category/<?php echo $image['name']; ?>" />
                                        <div class="imgControl">                      
                                            <div class="imgDel">
                                                <div url="/admin/category/delImage" idimg="<?php echo $image['id']; ?>"><a>Удалить</a></div>
                                            </div>
                                            <div class="imageCheck">
                                                <input id_img="<?php echo $image['id']; ?>" id_es="<?php echo $image['id_es']; ?>" <?php if ($image['main']) echo 'checked'; ?> type="radio" name="mainImg" data-toggle="tooltip"  data-placement="top" title="Выбрать главным" />
                                            </div>
                                        </div>

                                    </div>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </div>

                        <hr>

                        <button type="button" id="addImg" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                            Добавить
                        </button>

                        <!-- Модаль -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div id="modal_result_id"></div>
                                        <h4 class="modal-title" id="myModalLabel">Добавить изображение</h4>
                                    </div>
                                    <div class="modal-body">
                                        <label for="exampleInputFile">Изображение категории</label>
                                        <input name="img" type="file" id="inputFile">
                                        <p class="help-block">*Допустимые форматы PNG, JPG</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                        <button essence="category" id="uploadFile" type="button" class="btn btn-primary">Загрузить файл</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="help-block">*Изменения вносятся по клику (и тут же отображаются на сайте. Будьте внимательны!), нажимать сохранить не обязательно.</p>
                    </div>
                <?php else: ?>
                    <p class="alert alert-warning fade in">* Для добавления изображений сохраните категорию и перейдите к её редактированию</p>
                <?php endif; ?>
            </div>
        </div>

        <input type="hidden" name="id" value="<?php
        echo (isset($page['id'])) ? $page['id'] : '';
        ?>" />
    </form>



</div>