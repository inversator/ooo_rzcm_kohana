<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header"><?php echo $pTitle; ?></h1>

    <h2 class="sub-header">Список</h2>
    <div id="result_id"></div>
    <button class = "btn btn-success" type = "button" onclick="location.href = '/admin/category/new/';
            return true;">Добавить +</button>
    <div><?php echo isset($pagination) ? $pagination : ''; ?></div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Идентификатор</th>
                    <th>Имя клиента</th>
                    <th>Подробнее</th>
                    <th>Удалить</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $num = 0;
                foreach ($pages as $page): $num++;
                    ?>
                    <tr id='ItemDel-<?php echo $page['id']; ?>'>
                        <td><?php echo $num + ($listNum - 1) * 10; ?></td>
                        <td><?php echo $page['id']; ?></td>
                        <td><a href='/admin/order/view/<?php echo $page['id']; ?>'><?php echo $page['name']; ?></a></td>
                        <td><button class = "btn btn-primary" type = "button" onclick="location.href = '/admin/order/view/<?php echo $page['id']; ?>';
                                return true;">Редактировать</button>
                        </td>
                        <td><button 
                                class="btn btn-danger" 
                                onclick="DelObjectAjax('<?php echo $page['id']; ?>', '/admin/order/delete/', 'ItemDel-<?php echo $page['id']; ?>')
                                        return true;" type="button">Удалить</button></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo isset($pagination) ? $pagination : ''; ?>
    </div>
</div>