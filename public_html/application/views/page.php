<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-12">
            <div class="page-header row">
                <div class="col-xs-12 col-sm-4"><h1><?php echo $title; ?></h1></div>
                <div class="col-xs-12 col-sm-8"><p class="lead"><?php echo $desc; ?></p></div>
            </div>
            <hr>
            <div class="fullText"><?php echo $text; ?></div>
        </div>
    </div>
</div>