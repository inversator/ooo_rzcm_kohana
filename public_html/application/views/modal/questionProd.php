<div class="modal fade" id="questionProd" tabindex="-1" role="dialog" aria-labelledby="questionProd" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 style="display: inline-block; line-height: 30px; padding-left: 10px;" class="modal-title"
                    id="myModalLabel">Отправить резюме</h4>
                <img style="float: left; padding-left: 7px;" src="/public/images/resume.png" width="40"/>
            </div>

            <div class="modal-body">
                <form id="resume">
                    <div class="form-group" own="name">
                        <error></error>
                        <div class="input-group">
                            <input onchange="checkField('questionProd')" class="form-control" type="text1" name="name"
                                   value="" placeholder="Ваше имя">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                        </div>
                    </div>

                    <div class="form-group" own="phone">
                        <error></error>
                        <div class="input-group">
                            <input id="askMask" onchange="checkField('questionProd')" class="form-control" type="text1"
                                   name="phone"
                                   value="" placeholder="Ваш телефон">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>
                        </div>
                    </div>

                    <div class="form-group" own="email">
                        <error></error>
                        <div class="input-group">
                            <input onchange="checkField('questionProd')" class="form-control" type="text1" name="email"
                                   value="" placeholder="Ваше e-mail">
                            <span class="input-group-addon"> <span class="glyphicon glyphicon-envelope"></span></span>
                        </div>
                    </div>

<!--                    <div class="form-group" own="function">-->
<!--                        <p><strong>Интересующий товар/услуга</strong></p>-->
<!--                        <input type="text" value="Плавильщик" readonly="readonly" class="form-control text-center"-->
<!--                               name="function"/>-->
<!--                    </div>-->
<!--                    <div class="form-group" own="file">-->
<!--                        <error></error>-->
<!--                        <p><strong>Файл с резюме:</strong> </p>-->
<!--                        <div class="input-group">-->
<!--                            <input type="file" name="file" class="form-control"/>-->
<!--                            <span class="input-group-addon">-->
<!--                                <span class="glyphicon glyphicon-file"></span>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                    </div>-->

                    <div class="form-group" own="comment">
                        <error></error>
                        <strong>Сообщение:</strong>
                        <textarea class="form-control" rows="10" name="comment"
                                  onchange="checkField('questionProd')"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary">Отправить</button>
            </div>
        </div>
    </div>
</div>
<script src="/public/js/masked/jquery.maskedinput.js" type="text/javascript"></script>
<script>
    $(function () {
        $("#askMask").mask("8(999) 999-9999");
    });

    $RoomWrap('#resumeModal').on('show.bs.modal', function (e) {
            funcValue = $RoomWrap(e.relatedTarget).attr('function');
            $RoomWrap('input[name=function]').val(funcValue);
        }
    );

</script>