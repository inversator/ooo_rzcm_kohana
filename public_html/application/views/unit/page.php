<link rel="stylesheet" href="../public/jqzoom/css/jquery.jqzoom.css" type="text/css">
<script src="../public/jqzoom/js/jquery-1.6.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/public/css/thumbelina.css" />
<script type="text/javascript" src="/public/js/thumbelina.js"></script>
<script type="text/javascript">
    $(function () {

        $('#slider1').Thumbelina({
            $bwdBut: $('#slider1 .left'), // Selector to left button.
            $fwdBut: $('#slider1 .right')    // Selector to right button.
        });
    })
</script>

<div class="row row-offcanvas row-offcanvas-right">
    <div class="col-xs-12 col-sm-9">
        <p class="pull-right visible-xs">
            <button class="btn btn-primary btn-xs" data-toggle="offcanvas" type="button">Список категорий</button>
        </p>
        <div class="page-header">
            <h1><?php echo $title; ?></h1>
            <div class="row">
                <div class="unitImgBlock col-sm-6">
                    <div class="clearfix"> <a href="/public/images/unit/<?php echo $mainImage; ?>" class="jqzoom" rel='gal1' title="<?php echo $title; ?>"> <img src="/public/images/unit/<?php echo $mainImage; ?>" class="img-thumbnail mainImgUnit"> </a> </div>
                    <?php if (count($images) > 1): ?>
                        <br/>
                        <div class="clearfix" >
                            <div id="slider1">
                                <div class="thumbelina-but horiz left">&#706;</div>
                                <ul id="thumblist" class="clearfix" >
                                    <?php foreach ($images as $image): ?>
                                        <li class="img-thumbnail"> <a 
                                                href='javascript:void(0);' 
                                                rel="{gallery: 'gal1', smallimage: '../public/images/unit/<?php echo $image['name']; ?>',largeimage: '../public/images/unit/<?php echo $image['name']; ?>'}"> <img class="img-rounded" width='120' src='../public/images/unit/<?php echo $image['name']; ?>'> </a> </li>
                                        <?php endforeach; ?>
                                </ul>
                                <div class="thumbelina-but horiz right">&#707;</div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-sm-6">
                    <div class="mainUnitPrice"> <?php echo $price; ?> Рублей </div>
                    <a onclick="addToCart(<?php echo $id; ?>);
                                    return false;" class="btn btn-primary" role="button">В корзину</a>
                    <p class="lead"><?php echo $desc; ?></p>
                    <div class="properties">
                        <table class="table table-striped">
                            <?php foreach ($properties as $property): ?>
                                <tr>
                                    <td><?php echo $property['alias']; ?></td>
                                    <td><?php echo $property['value']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="fullText"><?php echo $text; ?></div>
        <?php if (isset($units) && count($units)): ?>
            <hr>
            <h3>Похожие товары:</h3>


            <?php if (count($units) > 3): ?>
                <div class="carousel slide" id="myCarousel">
                    <div class="carousel-inner">
                        <?php
                        $unitsChunk = array_chunk($units, 3);
                        $i = 0;
                        foreach ($unitsChunk as $arr) {
                            ?>
                            <div class="item<?php if (!$i) echo " active"; ?>">
                                <div class="thumbnails">
                                    <?php
                                    foreach ($arr as $unit):
                                        ?>

                                        <div class="col-sm-4 col-md-4">
                                            <div class="thumbnail unit">
                                                <div class="unitListImg"> <a href='<?php echo "/".$unit['url']; ?>'><img alt="" class="unitImg" src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a> </div>
                                                <div class="caption unitListBlock">
                                                    <h4 class="unitListName"><a href='<?php echo "/".$unit['url']; ?>'><?php echo Room::subTitle($unit['pTitle']); ?></a></h4>
                                                    <div class="unitListDesc"><?php echo Room::subDesc($unit['desc']); ?></div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                    endforeach;
                                    $i++;
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                    <div class="control-box">                            
                        <a data-slide="prev" href="#myCarousel" class="carousel-control left"> <img src="../../public/images/Arrow-Left-icon.png" /></a>
                        <a data-slide="next" href="#myCarousel" class="carousel-control right"> <img src="../../public/images/Arrow-Right-icon.png" /></a>
                    </div>
                </div>
            <?php else: ?>

                <div class="row">
                    <?php foreach ($units as $unit): ?>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail unit">
                                <div class="unitListImg"> <a href='<?php echo "/".$unit['url']; ?>'><img alt="" class="unitImg" src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a> </div>
                                <div class="caption unitListBlock">
                                    <h4 class="unitListName"><a href='<?php echo "/".$unit['url']; ?>'><?php echo $unit['pTitle']; ?></a></h4>
                                    <div class="unitListDesc"><?php echo Room::subDesc($unit['desc']); ?></div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

        <?php endif; ?>

    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation"> <?php echo $rigthMenu; ?> </div>
</div>
<script src="../public/jqzoom/js/jquery.jqzoom-core.js" type="text/javascript"></script> 
<script type="text/javascript">

                            $(document).ready(function () {
                                $('.jqzoom').jqzoom({
                                    zoomType: 'reverse',
                                    lens: true,
                                    preloadImages: false,
                                    alwaysOn: false
                                });
                            });
</script> 
