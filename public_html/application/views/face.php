</div>
<div id="slider">
    <div data-ride="carousel" class="carousel slide" id="carousel-example-captions">
        <!--        <ol class="carousel-indicators">-->
        <!--            <li class="active" data-slide-to="0" data-target="#carousel-example-captions"></li>-->
        <!--            <li data-slide-to="1" data-target="#carousel-example-captions" class=""></li>-->
        <!---->
        <!--        </ol>-->
        <div class="carousel-inner">
            <div class="item active"><img title="Рязцветмет" alt="Рязцветмет"
                                          src="/public/images/slider/1200/slide-1.png">

                <div class="carousel-caption">


                    <h3><?php echo __('High quality refined lead, lead alloys, calcium alloys.'); ?></h3>

                    <p><?php echo __('All goods comply with ISO standards.'); ?></p>
                </div>
            </div>
            <div class="item"><img alt="Рязцветмет" title="Рязцветмет"
                                   src="/public/images/slider/1200/slide-2.png">

                <div class="carousel-caption">
                    <h3><?php echo __('High quality products'); ?></h3>

                    <p><?php echo __('All products meet the world and Russian quality standards'); ?></p>
                </div>
            </div>

        </div>
        <a data-slide="prev" href="#carousel-example-captions" class="left carousel-control">
            <!--            <img src="/public/images/Arrow-Left-icon.png">-->
        </a> <a data-slide="next" href="#carousel-example-captions" class="right carousel-control">
            <!--            <img src="/public/images/Arrow-Right-icon.png">-->
        </a></div>
    <div class="container">
        <div class="row">
            <div class="factoryImage"><img src="/public/images/zavod.png"></div>
        </div>
    </div>
</div>
<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-5 zavodName">
            <div class="page-header jumbotron">

                <h2><?php echo $title; ?></h2>

                <p class="lead"><?php echo htmlspecialchars_decode($desc); ?></p>

            </div>
            <div class="fullText"><?php echo htmlspecialchars_decode($text); ?></div>
            <?php echo __('<p>
	 Ltd «Ryazcvetmet» is one of the major Russian lead producers. For its more than 55-year history the plant «Ryazcvetmet» has become the leader оf Russian metallurgy.
</p>
<p>
	 We have regular production of refined lead. We also produce the traditional lead alloys.
</p>
<p>
	 There is a modernly equipped laboratory in the factory, that allows us to distribute our products in compliance with both, the global market requirements and the Russian standards.
</p>	'); ?>

        </div>

        <div class="col-xs-7">
            <div id="hexagon">
                <ul>
                    <li>— <?php echo __('A wide range of certified products in all sizes'); ?>;</li>
                    <li>— <?php echo __('Supply of high-quality refined lead, lead alloys and of calcium alloys'); ?>;
                    </li>
                    <li>— <?php echo __('Purchase of scrap batteries'); ?>;</li>
                    <li>— <?php echo __('Realization of both standard alloys and alloys under individual order'); ?>;
                    </li>
                </ul>
            </div>
            <div class="afterHexagon">
                <p><?php echo __('The company has a laboratory equipped with modern technology, which allows
                     which allows to produce
                     products at the world level and fully compliant with Russian GOSTs.'); ?></p>

                <div class="row">
                    <div class="maxwidth-theme">
                        <div class="col-md-12">
                            <div class="banners-small front">
                                <div class="items row">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="item" id="bx_651765591_39">
                                            <div class="image">
                                                <img src="/public/images/face/755386ac70061edc6c69d129fd8c3ce0.png">
                                            </div>
                                            <div class="title" style="padding-top: 41px;">
                                                <a data-lk="#/services/audit/nalogovyy-audit/"><?php echo __('Wide range of goods'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="item" id="bx_651765591_40">
                                            <div class="image">
                                                <img src="/public/images/face/fd444aa7e52406a85ebc4655801f3eeb.png">
                                            </div>
                                            <div class="title" style="padding-top: 41px;">
                                                <a data-lk="#/services/audit/auditorskaya-proverka/"><?php echo __('Any tonnage'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="item" id="bx_651765591_41">
                                            <div class="image">
                                                <img src="/public/images/face/17edaa7182410445b63af1220f72f665.png">
                                            </div>
                                            <div class="title" style="padding-top: 33px;">
                                                <a data-lk="#/services/autsorsing/zashchita-informatsii/"><?php echo __('Shortest period of production'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="item" id="bx_651765591_42">
                                            <div class="image">
                                                <img src="/public/images/face/077afcd1729357ea95b1ee7bfd0fc8a7.png">
                                            </div>
                                            <div class="title" style="padding-top: 33px;">
                                                <a data-lk="#/services/autsorsing/soprovozhdenie-1s/"><?php echo __('All products are certified'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-xs-12 col-sm-12 face">

        </div>
    </div>
</div>
<div class="fixFon">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="consultation">
                    <h3><?php echo __('CONSULTATION'); ?></h3>

                    <p><?php echo __('The managers of the company will gladly answer your questions and prepare an individual commercial offer.'); ?></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 pull-right">
                <div class="consultBtn">
                    <div class="btn btn-lg btn-warning" data-target="#contactModal" data-toggle="modal">
                        <?php echo __('Ask a Question'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">


    <div>
        <div class="textContent">
            <div class="row">
                <div class="col-xs-7">
                    <div id="hexagonInverse" class="row">
                        <div class="faceProducts">
                            <h2><?php echo __('Products'); ?></h2>

                            <p><?php echo __('High-quality products'); ?></p>
                            <hr>
                            <h5><?php echo __('Lead'); ?></h5>

                            <div class="row">

                                <div class="col-xs-9">


                                    <p><?php echo __('High-quality refined lead and lead alloys. We can produce lead alloys and calcium alloys as per the customer’s requirement. All goods comply with ISO standards.'); ?>
                                    </p>
                                </div>

                                <div class="col-xs-2 text-right">
                                    <img class="img-circle"
                                         src="/public/images/face/2432663978ebd8f7f1d682051c47de60.jpg"/>
                                </div>
                            </div>

                            <hr>

                            <!--                            <div class="row">-->
                            <!---->
                            <!--                                <div class="col-xs-9">-->
                            <!--                                    <h5>Сульфат натрия</h5>-->
                            <!---->
                            <!--                                    <p>-->
                            <!--                                        Мы поставляем высококачественный сульфат натрия, соответствующий-->
                            <!--                                        российским стандартам.-->
                            <!--                                    </p>-->
                            <!--                                </div>-->
                            <!--                                <div class="col-xs-2 text-right">-->
                            <!--                                    <img class="img-circle" src="/public/images/face/f913ddd055df41de766e338f632c663b.jpg"/>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <hr>
                            <div class="row">

                                <div class="col-xs-9">
                                    <h5><?php echo __('Polypropylene (PP)'); ?></h5>

                                    <p><?php echo __('Wide range of recycled polypropylene'); ?></p>
                                </div>
                                <div class="col-xs-2 text-right">
                                    <img class="img-circle"
                                         src="/public/images/face/8e8f387ed97d84559387f6907420645b.jpg"/>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="afterHexagonInverse row">

                    </div>


                </div>
                <div class="col-xs-5">
                    <!-- Блок Последних новостей -->
                    <?php if (isset($newsList) && count($newsList)): ?>
                        <h3 class="center"><?php echo __('NEWS'); ?></h3>
                        <?php if (count($newsList) > 4): ?>
                            <div class="carousel slide" id="LastUnitCar">
                                <div class="carousel-inner">
                                    <?php
                                    $unitsChunk = array_chunk($newsList, 4);
                                    $i = 0;
                                    foreach ($unitsChunk as $arr) {
                                        ?>
                                        <div class="item<?php if (!$i) echo " active"; ?>">
                                            <div class="thumbnails">
                                                <?php
                                                foreach ($arr as $unit):
                                                    ?>
                                                    <div class="col-sm-3 col-md-6">
                                                        <div class="thumbnail unit">
                                                            <!--                                                            <div class="unitListImg"><a-->
                                                            <!--                                                                    href='-->
                                                            <?php //echo "/news/" . $unit['url'];
                                                            ?><!--'><img-->
                                                            <!--                                                                        alt="" class="unitImg"-->
                                                            <!--                                                                        src="/public/images/unit/-->
                                                            <?php //echo Room::showImg($unit['image']);
                                                            ?><!--"></a>-->
                                                            <!--                                                            </div>-->
                                                            <div class="caption unitListBlock">
                                                                <h4 class="unitListName faceNewsName"><a
                                                                            href='<?php echo "/news/" . $unit['url']; ?>'><?php echo Room::subTitle($unit['pTitle']); ?></a>
                                                                </h4>

                                                                <div
                                                                        class="unitListDesc"><?php echo htmlspecialchars_decode(Room::subDesc($unit['desc'])); ?></div>
                                                                <div class="unitButtons"><a
                                                                            href='<?php echo "/news/" . $unit['url']; ?>'
                                                                            role="button"
                                                                            class="btn btn-primary"><?php echo __('More'); ?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                endforeach;
                                                $i++;
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="control-box"><a data-slide="prev" href="#LastUnitCar"
                                                            class="carousel-control left">
                                        <img
                                                src="../../public/images/Arrow-Left-icon.png"/></a> <a data-slide="next"
                                                                                                       href="#LastUnitCar"
                                                                                                       class="carousel-control right">
                                        <img
                                                src="../../public/images/Arrow-Right-icon.png"/></a></div>
                            </div>
                        <?php else: ?>
                            <div class="row">
                                <?php foreach ($newsList as $unit): ?>
                                    <div class="col-sm-3 col-md-6">
                                        <div class="thumbnail unit">
                                            <!--                                            <div class="unitListImg"><a href='-->
                                            <?php //echo "/news/" . $unit['url']; ?><!--'><img-->
                                            <!--                                                        alt="" class="unitImg"-->
                                            <!--                                                        src="/public/images/unit/-->
                                            <?php //echo Room::showImg($unit['image']); ?><!--"></a>-->
                                            <!--                                            </div>-->
                                            <div class="caption unitListBlock">
                                                <h4 class="unitListName faceNewsName"><a
                                                            href='<?php echo "/news/" . $unit['url']; ?>'><?php echo Room::subTitle($unit['pTitle']); ?></a>
                                                </h4>

                                                <div
                                                        class="unitListDesc"><?php echo htmlspecialchars_decode(Room::subDesc($unit['desc'])); ?></div>
                                                <div class="unitButtons"><a
                                                            href='<?php echo "/news/" . $unit['url']; ?>'
                                                            role="button"
                                                            class="btn btn-primary"><?php echo __('More'); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                        <hr>
                        <div class="mainAllNews center"><a href="/news"
                                                           class="btn btn-warning"><?php echo __('Show all'); ?></a>
                        </div>
                    <?php endif; ?>

                </div>

            </div>
        </div>
    </div>


</div>
</div>
<script>
    function activeTab(num) {
        $RoomWrap('#myTab li:eq(' + num + ') a').tab('show');
    }
</script> 