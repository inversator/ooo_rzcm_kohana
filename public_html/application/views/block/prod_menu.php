<div class="menuHeader"><?php echo __('Products'); ?></div>

<div class="list-group">
    <?php Menu::bList($list); ?>
</div>

<script>
    $(document).ready(function () {
        $('[data-toggle=offcanvas]').click(function () {
            $('.row-offcanvas').toggleClass('active')
        });
    });
</script>

