<div id="scroll-menu" class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#midMenu">
                <span class="sr-only"><?php echo __('menu'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="midMenu">
            <!--
            <ul class="nav navbar-nav nav-pills">
                <?php echo $lis; ?>
            </ul>
-->
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"
                       aria-expanded="false"><?php echo __('company') ?><b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/company"><?php echo __('about company') ?></a></li>
                        <li class="devider" role="separator"></li>
                        <li><a href="/company/history"><?php echo __('history'); ?></a></li>
                        <li><a href="/company/factory"><?php echo __('The plant'); ?></a></li>
                        <li><a href="/company/staff"><?php echo __('The staff'); ?></a></li>
                        <li><a href="/company/licenses"><?php echo __('certificates'); ?></a></li>
                        <li><a href="/company/promcare"><?php echo __('industrial safety'); ?></a></li>
                        <?php if (I18n::lang() == 'ru'): ?>
                            <li><a href="/vacancy"><?php echo __('vacancy'); ?></a></li>
                        <?php endif; ?>
                        <li><a href="/company/requisites"><?php echo __('requisites'); ?></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="/catalog"
                       aria-expanded="false"><?php echo __('products'); ?><b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/catalog/svinets"><?php echo __('lead'); ?></a></li>
                        <li><a href="/catalog/polipropilen"><?php echo __('polypropylene'); ?></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="/cooperation"
                       aria-expanded="false"><?php echo __('cooperation'); ?><b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/buyers"><?php echo __('for сustomers'); ?></a></li>
                        <li><a href="/providers"><?php echo __('for suppliers'); ?></a></li>
                        <!--                        <li><a href="sulfat">Статьи</a></li>-->
                    </ul>
                </li>
                <li><a href="/ecology"><?php echo __('ecology'); ?></a></li>
                <li><a href="/news"><?php echo __('news'); ?></a></li>
                <li><a href="/contacts"><?php echo __('contact us'); ?></a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="right"><a href="<?php echo Kohana::$config->load('main.site-ru');?>"><img src="/public/images/ru.png"/></a></li>
                <li class="right"><a href="<?php echo Kohana::$config->load('main.site-en');?>"><img src="/public/images/en.png"/></a></li>

            </ul>
        </div>
    </div>
</div>