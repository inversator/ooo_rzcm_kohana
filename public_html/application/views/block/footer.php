<div id="infoBlock">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="logo-bottom">
                    <img width="110" class="imageRubber" src="/public/images/logo-<?php echo I18n::lang();?>.png"/>
                </div>
                <hr>
                <p><?php echo __('Russian supplier of lead and lead alloys'); ?></p>
                <p><a target="_blank" href="/public/docs/protection.pdf"><?php echo __('Policy regarding the processing of personal data'); ?></a></p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <h3><?php echo __('Navigation'); ?></h3>
                <hr>
                <div class="row">
                    <div class="col-xs-6">
                        <ul class="list-unstyled">
                            <li><a href="/catalog" class=""><?php echo __('products'); ?></a></li>
                            <li><a href="/company" class=""><?php echo __('company'); ?></a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <ul class="list-unstyled">
                            <li><a href="/buyers" class=""><?php echo __('for сustomers'); ?></a></li>
                            <li><a href="/providers" class=""><?php echo __('for suppliers'); ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <h3><?php echo __('Contact Information'); ?></h3>
                <hr>
                <ul class="list-unstyled list">
                    <li>
                        <span class="glyphicon glyphicon-map-marker"></span>&nbsp;<?php echo __('305025, Kursk, Directions Magistralny, d. 18T, office 5'); ?>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-earphone"></span>&nbsp; +7 (4912) 95-67-56
                    </li>
                    <!--li>
                        <span class="glyphicon glyphicon-send"></span>&nbsp;<ahref="mailto:rzcm@rzcm.ru">rzcm@rzcm.ru</a>
                    </li-->
                </ul>
            </div>
        </div>
    </div>
</div>

<footer id="footer">
    <div class="container">
        <div class="row">
            <span class="copypast col-sm-12 col-xs-12 text-center">
                <p><?php echo __('We will be glad to start cooperation with you!'); ?></p>
            </span>
        </div>
    </div>
</footer>
