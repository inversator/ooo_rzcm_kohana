<div class="container">
    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div id="topInfoText">
                <span class="glyphicon glyphicon-send"></span>&nbsp;<a href="mailto:rzcm@rzcm.ru">rzcm@rzcm.ru</a><br>
                <span class="glyphicon glyphicon-map-marker"></span> <?php echo __('305025, Kursk, Directions Magistralny, d. 18T, office 5'); ?><br>
                <span class="glyphicon glyphicon-time"></span> <?php echo __('Office hours :  9:00 - 18:00'); ?>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4 text-center">
            <a class="logo" href="/" title="Рязцветмет">
                <img class="imageRubber" src="/public/images/logo-<?php echo I18n::lang();?>.png" title="Рязцветмет" alt="Рязцветмет"/>
            </a>
        </div>

        <div class="col-xs-12 col-sm-4 headBlock3">
            <ul class="nav navbar-nav navbar-right">
                <li class="topPhone">
                    <span><span class="glyphicon glyphicon-earphone"></span>&nbsp; +7 (4912) 95-67-56</span>
                </li>
                <li class="callMe">
                    <button class="btn btn-warning" data-target="#callmeModal" data-toggle="modal"><?php echo __('Callback'); ?>
                    </button>
                </li>
            </ul>
        </div>

    </div>
</div>