<div id="staff">
    <div class="row">
        <h2>Engineers</h2>
        <div class="col-xs-12 col-sm-4">
            <div class="block"><img class="img img-circle" src="../../../public/images/upload/staff/da49c4e3057d44b021590495fcd56385.jpg" alt="" />
                <div class="fio">Bobychev Gennady Vasilyevich</div>
                <div class="function">Chief Power Engineer</div>
                <hr />
                <div class="email"><a href="mailto:a.titashin@rzcm.ru">g.bobychev@rzcm.ru</a> &nbsp;<span class="glyphicon glyphicon-send"></span></div>
                <div class="phone">+7 (4912) 95-67-84 &nbsp;<span class="glyphicon glyphicon-phone"></span></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="block"><img class="img img-circle" src="../../../public/images/upload/staff/tatishin.jpg" alt="" />
                <div class="fio">Tatishin Aleksander</div>
                <div class="function">Chief mechanical engineer</div>
                <hr />
                <div class="email"><a href="mailto:a.titashin@rzcm.ru">a.titashin@rzcm.ru</a> &nbsp;<span class="glyphicon glyphicon-send"></span></div>
                <div class="phone">+7 (4912) 95-67-65 &nbsp;<span class="glyphicon glyphicon-phone"></span></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="block"><img class="img img-circle" src="../../../public/images/upload/staff/kolobin.jpg" alt="" />
                <div class="fio">Kolobin Mikhail</div>
                <div class="function">Head of transport department</div>
                <hr />
                <div class="email"><a href="mailto:m.kolobin@rzcm.ru">m.kolobin@rzcm.ru</a> &nbsp;<span class="glyphicon glyphicon-send"></span></div>
                <div class="phone">+7 (4912) 95-67-82 &nbsp;<span class="glyphicon glyphicon-phone"></span></div>
            </div>
        </div>
    </div>
    <div class="row">
        <h2>IT Department</h2>
        <div class="col-xs-12 col-sm-4">
            <div class="block">
                <div class="fio">Semushkin Alexander</div>
                <div class="function">Head of Department</div>
                <hr />
                <div class="email"><a href="mailto:e.mryhin@rzcm.ru">a.semushkin@rzcm.ru</a> &nbsp;<span class="glyphicon glyphicon-send"></span></div>
                <div class="phone">+7 (4912) 95-67-44 &nbsp;<span class="glyphicon glyphicon-phone"></span></div>
            </div>
        </div>
    </div>
    <div class="row">
        <h2>Sales department</h2>
        <div class="col-xs-12 col-sm-4">
            <div class="block"><img class="img img-circle" src="../../../public/images/upload/staff/pavlov.jpg" alt="" />
                <div class="fio">Pavlov Yuriy</div>
                <div class="function">Lead international manager</div>
                <hr />
                <div class="email"><a href="mailto:customs@rzcm.ru">customs@rzcm.ru</a> &nbsp;<span class="glyphicon glyphicon-send"></span></div>
                <div class="phone">+7 (4912) 95-67-46 &nbsp;<span class="glyphicon glyphicon-phone"></span></div>
            </div>
        </div>
    </div>
    <div class="row">
        <h2>Estimating department</h2>
        <div class="col-xs-12 col-sm-4">
            <div class="block"><img class="img img-circle" src="../../../public/images/upload/staff/yanbaeva.jpg" alt="" />
                <div class="fio">Yanbaeva Natalia</div>
                <div class="function">Lead economist</div>
                <hr />
                <div class="email"><a href="mailto:n.yanbaeva@rzcm.ru">n.yanbaeva@rzcm.ru</a> &nbsp;<span class="glyphicon glyphicon-send"></span></div>
                <div class="phone">+7 (4912) 95-67-77 &nbsp;<span class="glyphicon glyphicon-phone"></span></div>
            </div>
        </div>

    </div>
    <div class="row">
        <h2>Production and technical department</h2>
        <div class="col-xs-12 col-sm-4">
            <div class="block"><img class="img img-circle" src="../../../public/images/upload/staff/koval.jpg" alt="" />
                <div class="fio">Koval Nikolay</div>
                <div class="function">Head of production and technical department</div>
                <hr />
                <div class="email"><a href="mailto:n.koval@rzcm.ru">n.koval@rzcm.ru</a> &nbsp;<span class="glyphicon glyphicon-send"></span></div>
                <div class="phone">+7 (4912) 95-67-37 &nbsp;<span class="glyphicon glyphicon-phone"></span></div>
            </div>
        </div>
    </div>
</div>