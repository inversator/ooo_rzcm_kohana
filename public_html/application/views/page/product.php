<link rel="stylesheet" href="/public/jqzoom/css/jquery.jqzoom.css" type="text/css">
<script src="/public/jqzoom/js/jquery-1.6.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/public/css/thumbelina.css"/>
<script type="text/javascript" src="/public/js/thumbelina.js"></script>
<script type="text/javascript">
    $(function () {
        $('#slider1').Thumbelina({
            $bwdBut: $('#slider1 .left'), // Selector to left button.
            $fwdBut: $('#slider1 .right')    // Selector to right button.
        });
    })
</script>
<div class="breadcrumbs">
    <div class="container">

        <a href="/catalog/"><?php echo __('Products'); ?></a>
        <span class="glyphicon glyphicon-chevron-right"></span>
        <a href="/catalog/<?php echo $category['url']; ?>"><?php echo $category['pTitle_' . I18n::lang()]; ?></a>
        <span class="glyphicon glyphicon-chevron-right"></span>
        <a href="/catalog/<?php echo $self['url']; ?>"><?php echo $self['pTitle_' . I18n::lang()]; ?></a>

    </div>

</div>
<div id="product">
    <div class="container">
        <div class="row row-offcanvas row-offcanvas-right">
            <div class="col-xs-12 col-sm-9">
                <p class="pull-right visible-xs">
                    <button class="btn btn-primary btn-xs" data-toggle="offcanvas" type="button">Продукция</button>
                </p>
                <div class="page-header">
                    <h1><?php echo $self['pTitle_' . I18n::lang()]; ?></h1>

                    <script type="text/javascript">(function (w, doc) {
                            if (!w.__utlWdgt) {
                                w.__utlWdgt = true;
                                var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                s.type = 'text/javascript';
                                s.charset = 'UTF-8';
                                s.async = true;
                                s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                                var h = d[g]('body')[0];
                                h.appendChild(s);
                            }
                        })(window, document);
                    </script>

                    <div data-background-alpha="0.0" data-buttons-color="#ffffff"
                         data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false"
                         data-share-counter-type="disable" data-share-style="6" data-mode="share"
                         data-like-text-enable="false" data-mobile-view="true" data-icon-color="#ffffff"
                         data-orientation="horizontal" data-text-color="#000000" data-share-shape="rectangle"
                         data-sn-ids="fb.vk.tw.ok." data-share-size="30" data-background-color="#ffffff"
                         data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1635358"
                         data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true"
                         data-selection-enable="true" class="uptolike-buttons">
                    </div>

                    <div class="row">
                        <div class="unitImgBlock col-sm-6">
                            <div class="clearfix">
                                <a href="/public/images/unit/<?php echo $mainImage; ?>"
                                   class="jqzoom"
                                   rel='gal1' title="<?php echo $self['pTitle_' . I18n::lang()]; ?>"> <img
                                            src="/public/images/unit/<?php echo $mainImage; ?>"
                                            class="img-thumbnail mainImgUnit"> </a>
                            </div>
                            <?php if (count($images) > 1): ?>
                                <br/>
                                <div class="clearfix">
                                    <div id="slider1">
                                        <div class="thumbelina-but horiz left">&#706;</div>
                                        <ul id="thumblist" class="clearfix">
                                            <?php foreach ($images as $image): ?>
                                                <li class="img-thumbnail"><a
                                                            href='javascript:void(0);'
                                                            rel="{gallery: 'gal1', smallimage: '/public/images/unit/<?php echo $image['name']; ?>',largeimage: '/public/images/unit/<?php echo $image['name']; ?>'}">
                                                        <img class="img-rounded" width='120'
                                                             src='/public/images/unit/<?php echo $image['name']; ?>'>
                                                    </a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <div class="thumbelina-but horiz right">&#707;</div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="descUnderImg"> <?php echo htmlspecialchars_decode($self['desc_' . I18n::lang()]); ?></div>
                        </div>
                        <div class="col-sm-6">
                            <?php echo htmlspecialchars_decode($self['text_' . I18n::lang()]); ?>
                            <!--                            <div class="btn btn-default center" data-toggle="modal" data-target="#questionProd">Задать вопрос</div>-->
                        </div>
                    </div>
                </div>
                <div class="fullText"></div>
                <?php if (isset($units) && count($units)): ?>
                    <hr>
                    <h3><?php echo __('Similar products');?>:</h3>
                    <?php if (count($units) > 3): ?>
                        <div class="carousel slide" id="myCarousel">
                            <div class="carousel-inner">
                                <?php
                                $unitsChunk = array_chunk($units, 3);
                                $i = 0;
                                foreach ($unitsChunk as $arr) {
                                    ?>
                                    <div class="item<?php if (!$i) echo " active"; ?>">
                                        <div class="thumbnails">
                                            <?php
                                            foreach ($arr as $unit):
                                                ?>

                                                <div class="col-sm-4 col-md-4">
                                                    <div class="thumbnail unit">
                                                        <div class="unitListImg"><a
                                                                    href='<?php echo "/catalog/" . $category['url'] . "/" . $unit['url']; ?>'><img
                                                                        alt=""
                                                                        class="unitImg"
                                                                        src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a>
                                                        </div>
                                                        <div class="caption unitListBlock">
                                                            <h4 class="unitListName"><a
                                                                        href='<?php echo "/catalog/" . $category['url'] . "/" . $unit['url']; ?>'><?php echo Room::subTitle($unit['pTitle_' . I18n::lang()]); ?></a>
                                                            </h4>

                                                            <div
                                                                    class="unitListDesc"><?php echo htmlspecialchars_decode(Room::subDesc($unit['desc_' . I18n::lang()], 200)); ?></div>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php
                                            endforeach;
                                            $i++;
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                            <div class="control-box">
                                <a data-slide="prev" href="#myCarousel" class="carousel-control left"> <img
                                            src="../../public/images/Arrow-Left-icon.png"/></a>
                                <a data-slide="next" href="#myCarousel" class="carousel-control right"> <img
                                            src="../../public/images/Arrow-Right-icon.png"/></a>
                            </div>
                        </div>
                    <?php else: ?>

                        <div class="row">
                            <?php foreach ($units as $unit): ?>
                                <div class="col-sm-6 col-md-4">
                                    <div class="thumbnail unit">
                                        <div class="unitListImg"><a
                                                    href='<?php echo "/catalog/" . $category['url'] . "/" . $unit['url']; ?>'><img
                                                        alt=""
                                                        class="unitImg"
                                                        src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a>
                                        </div>
                                        <div class="caption unitListBlock">
                                            <h4 class="unitListName"><a
                                                        href='<?php echo "/catalog/" . $category['url'] . "/" . $unit['url']; ?>'><?php echo $unit['pTitle_' . I18n::lang()]; ?></a>
                                            </h4>

                                            <div
                                                    class="unitListDesc"><?php echo htmlspecialchars_decode(Room::subDesc($unit['desc_' . I18n::lang()])); ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                <?php endif; ?>

            </div>
            <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar"
                 role="navigation"> <?php echo $rigthMenu; ?> </div>
        </div>
    </div>

    <script src="/public/jqzoom/js/jquery.jqzoom-core.js" type="text/javascript"></script>

    <script type="text/javascript">

        $RoomWrap(document).ready(function () {
            $('.jqzoom').jqzoom({
                zoomType: 'reverse',
                lens: true,
                preloadImages: false,
                alwaysOn: false
            });
        });


    </script>

</div>

<?php echo $modal; ?>