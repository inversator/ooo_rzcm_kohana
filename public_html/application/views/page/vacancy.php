<div id="vacancy">
    <div class="container">
        <h1>Вакансии</h1>

        <div class="fullText">
            <?php echo htmlspecialchars_decode($self['desc']); ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel-group" id="accordion">
                    <?php
                    $in = true;
                    foreach ($vacancys as $vacancy): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse-<?php echo $vacancy['id']; ?>">
                                        <div class="function"><?php echo $vacancy['pTitle']; ?></div>
                                        <div class="text-right block"><?php echo $vacancy['bTitle']; ?></div>
                                    </a>
                                </h4>
                            </div>

                            <div id="collapse-<?php echo $vacancy['id']; ?>"
                                 class="panel-collapse collapse <?php //if($in) echo "in";?>">
                                <div class="panel-body">
                                    <?php echo htmlspecialchars_decode($vacancy['desc']); ?>
                                    <div class="properties">
                                        <hr/>
                                        <div class="property"><img
                                                src="/public/images/rouble.png"/>&nbsp; <?php echo $vacancy['bTitle']; ?>
                                        </div>
                                    </div>
                                    <p>
                                        <button class="btn btn-primary btn-lg" data-toggle="modal"
                                                data-target="#resumeModal"
                                                function="<?php echo $vacancy['pTitle']; ?>">
                                            Отправить резюме
                                        </button>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php
                        $in = false;
                    endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $modal; ?>

