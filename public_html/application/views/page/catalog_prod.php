<div id="catalog">
    <div class="container">
        <h1><?php echo $self['pTitle_' . I18n::lang()]; ?></h1>
        <div class="fullText">
            <?php echo htmlspecialchars_decode($self['desc_' . I18n::lang()]); ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="licenses">
                    <div class="row">
                        <h2><?php echo __('High-quality goods from the producer');?>: </h2>
                        <?php foreach ($units as $id => $unit): ?>
                            <div class="col-xs-12 col-sm-4">
                                <div class="block">
                                    <div class="licensesImage">
                                        <a href="<?php echo '/catalog/' . $self['url'] . '/' . $unit['url'];?>">
                                            <img class="imageRubber"
                                                 src="/public/images/unit/<?php echo $unit['image']; ?>"
                                                 alt=""/>
                                        </a>
                                    </div>
                                    <div class="title"><?php echo $unit['pTitle_' . I18n::lang()]; ?></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
