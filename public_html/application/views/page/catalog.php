<div id="vacancy">
    <div class="container">
        <h1><?php echo $self['pTitle_' . I18n::lang()]; ?></h1>

        <div class="fullText">
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="licenses">
                    <div class="row">
                        <h2><?php echo __('High-quality goods from the producer');?>: </h2>
<!--                        <div class="col-xs-12 col-sm-4">-->
<!--                            <div class="block">-->
<!--                                <div class="licensesImage"><a href="#"> <img class="imageRubber" src="/public/images/upload/page/sulfat.jpg" alt="" /></a></div>-->
<!--                                <div class="title">Сульфат натрия</div>-->
<!--                                <div class="desc">Мы производим высококачественный сульфат натрия, соответствующий российским стандартам.</div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="col-xs-12 col-sm-4">
                            <div class="block">
                                <div class="licensesImage"><a href="/catalog/svinets"> <img class="imageRubber" src="/public/images/upload/page/plumbum.jpg" alt="" /></a></div>
                                <div class="title"><?php echo __('Lead'); ?></div>
                                <div class="desc"><?php echo __('High-quality refined lead and lead alloys. We can produce lead alloys and calcium alloys as per the customer’s requirement. All goods comply with ISO standards.');?></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="block">
                                <div class="licensesImage"><a href="/catalog/polipropilen"> <img class="imageRubber" src="/public/images/upload/page/propilen.jpg" alt="" /></a></div>
                                <div class="title"><?php echo __('Polypropylene (PP)');?></div>
                                <div class="desc"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

