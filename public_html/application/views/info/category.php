<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button class="btn btn-primary btn-xs" data-toggle="offcanvas" type="button">Инфо</button>
            </p>

            <div class="page-header">
                <h1><?php echo $title; ?></h1>

                <p class="lead"><?php echo htmlspecialchars_decode($desc); ?></p>
            </div>

            <?php if (count($units)): ?>
                <div class="row">
                    <div class="filter" slug="<?php echo $slug; ?>" cat="<?php echo $id; ?>">
                    </div>
                    <div class="paginationBlock"><?php echo $pagination; ?></div>
                    <hr>

                    <div id="unitsBlock">
                        <?php foreach ($units as $unit):
                            ?>
                            <div class="info row">
                                <div class="infoListImg col-xs-12 col-sm-3">
                                    <a href='<?php echo "/" . $url . "/" . $unit['url']; ?>'><img alt="" class="unitImg"
                                                                                                  src="/public/images/unit/<?php echo Room::showImg($unit['image']); ?>"></a>
                                </div>

                                <div class="infoListBlock col-xs-12 col-sm-9">

                                    <h4 class="infoListName">
                                    <span class="label label-info"><?php
                                        echo date('d m Y', $unit['date']);
                                        ?></span>
                                        <a href='<?php echo "/" . $url . "/" . $unit['url']; ?>'><?php echo $unit['pTitle']; ?></a>
                                    </h4>

                                    <div
                                        class="infoListDesc"><?php echo htmlspecialchars_decode($unit['desc']); ?></div>
                                    <div class="infoButtons pull-right">
                                        <a href="<?php echo "/" . $url . "/" . $unit['url']; ?>">Читать</a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            <?php else: ?>
                <p class="alert alert-warning">Здесь пока ничего не написано</p>
            <?php endif; ?>

            <div class="paginationBlock"><?php echo $pagination; ?></div>
            <div class="fullText"><?php echo htmlspecialchars_decode($text); ?></div>
        </div>

        <div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
            <?php echo $rigthMenu; ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // Снимаем все чекбоксы
        $('.filter input:checkbox').attr('checked', false);
    })
</script>
