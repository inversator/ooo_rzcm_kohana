<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a href="#info" data-toggle="tab">Инфо</a></li>
    <li><a href="#profile" data-toggle="tab">Профиль</a></li>
    <li><a href="#orders" data-toggle="tab">Заказы</a></li>
    <li><a href="/users/logout">Выйти из профиля</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="info">
        <p class="pad15">Благодарим за регистрацию на нашем сайте. 
            Для просмотра списка заказов совершенных с Вашего email адреса, перейдите на вкладку "Заказы"</p>
    </div>
    <div class="tab-pane" id="profile">
        <table class="table">
            <tr><td>Идентификатор</td><td><?php echo $user->id; ?></td></tr>
            <tr><td>Логин</td><td><?php echo $user->username; ?></td></tr>
            <tr><td>Email</td><td><?php echo $user->email; ?></td></tr>
        </table>
    </div>
    <div class="tab-pane" id="orders">
        <div class="panel-group" id="accordion">
            <?php foreach ($orders as $order): ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $order['id']; ?>">
                                <?php echo $order['datetime']; ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse<?php echo $order['id']; ?>" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <?php
                                $sum = 0;
                                foreach ($order['units'] as $unit):
                                    ?>
                                    <tr>
                                        <td><a href="/<?php echo $unit['url'];?>"><?php echo $unit['pTitle']; ?></a> x <?php echo $unit['count']; ?></td>
                                        <td><?php
                                            echo $unit['value'] * $unit['count'];
                                            $sum += $unit['value'] * $unit['count'];
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td>Итого:</td>
                                    <td><?php echo $sum; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>
